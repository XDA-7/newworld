﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld
{
    public interface IWindowDimensions
    {
        int Width { get; }
        int Height { get; }
    }

    public class WindowDimensions : IWindowDimensions
    {
        public int Width
        {
            get
            {
                return 1000;
            }
        }

        public int Height
        {
            get
            {
                return 1000;
            }
        }
    }
}
