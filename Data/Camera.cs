﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

namespace NewWorld.Data
{
    public class Camera
    {
        public GameObject GameObject { get; set; }

        public Matrix4 View { get; set; }

        public bool ViewValid { get; set; }

        public Matrix4 Projection { get; set; }

        public float MinDist { get; set; }

        public float MaxDist { get; set; }

        public bool Active { get; set; }
    }
}
