﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    public class UserInput
    {
        /// <summary>
        /// 1-indexed
        /// </summary>
        public int Lightmap { get; set; }

        /// <summary>
        /// 1-indexed
        /// </summary>
        public int Depthmap { get; set; }

        /// <summary>
        /// 1-indexed
        /// </summary>
        public int DepthmapCubeSide { get; set; }
    }
}
