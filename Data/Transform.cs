﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

namespace NewWorld.Data
{
    public class Transform
    {
        public IList<GameObject> GameObjects { get; set; }

        public Vector3 Position { get; set; }

        public Quaternion Rotation { get; set; }

        public float Scale { get; set; }

        public Matrix4 Model { get; set; }

        /// <summary>
        /// The Model matrix is invalidated if the Transform has been moved/rotated/scaled
        /// </summary>
        public bool ModelValid { get; set; }
    }
}
