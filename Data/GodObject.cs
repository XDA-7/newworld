﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    /// <summary>
    /// Holds constant components which do not belong to any one object but to the scene as a whole
    /// </summary>
    public class GodObject
    {
        public Clock GlobalClock { get; set; }

        public Shader ShadowmapShader { get; set; }

        public Shader LightmapShader { get; set; }

        public Shader FrameCombineShader { get; set; }

        public Shader SingleFrameShader { get; set; }

        public Shader CubeFrameShader { get; set; }

        public VAOSet RenderQuad { get; set; }

        public UserInput UserInput { get; set; }

        public Texture BlankNormalMap { get; set; }

        public TextureGenerators TextureGenerators { get; set; }
    }

    public class TextureGenerators
    {
        public Shader NoiseGenerator { get; set; }
    }
}
