﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    public class ProceduralTexture
    {
        public IList<GameObject> GameObjects { get; set; }

        public FrameBuffer TextureBuffer { get; set; }
    }
}
