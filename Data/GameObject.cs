﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    /// <summary>
    /// Groups items of data together
    /// </summary>
    public class GameObject
    {
        public Transform Transform { get; set; }

        public Shader Shader { get; set; }

        public VAOSet VAOSet { get; set; }

        public Camera Camera { get; set; }

        public Velocity Velocity { get; set; }

        public Clock Clock { get; set; }

        public PointLight PointLight { get; set; }

        public Texture Texture { get; set; }

        public FrameBuffer FrameBuffer { get; set; }

        public Material Material { get; set; }

        public ProceduralTexture ProceduralTexture { get; set; }
    }
}
