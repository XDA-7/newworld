﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL4;

namespace NewWorld.Data
{
    public class Texture
    {
        public IList<GameObject> GameObjects { get; set; }

        public int TextureId { get; set; }

        public TextureTarget TextureTarget { get; set; }

        public PixelFormat PixelFormat { get; set; }

        public TextureWrapMode TextureWrapMode { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }
    }
}
