﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

namespace NewWorld.Data
{
    public class PointLight
    {
        public GameObject GameObject { get; set; }

        public Color Color { get; set; }

        public float Intensity { get; set; }

        public int ShadowmapResolution { get; set; }

        public FrameBuffer Shadowmap { get; set; }

        public FrameBuffer Lightmap { get; set; }
    }
}
