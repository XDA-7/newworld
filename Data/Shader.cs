﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    public class Shader
    {
        public IList<GameObject> GameObjects { get; set; }

        public int ProgramId { get; set; }

        public IDictionary<UniformType, int> Uniforms { get; set; }
    }
}
