﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    public class Clock
    {
        public IList<GameObject> GameObjects { get; set; }

        public bool Active { get; set; }

        public DateTime StartTime { get; set; }

        /// <summary>
        /// Seconds since last update
        /// </summary>
        public float DeltaTime { get; set; }

        public DateTime LastTick { get; set; }
    }
}
