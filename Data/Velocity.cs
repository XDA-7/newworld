﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

namespace NewWorld.Data
{
    public class Velocity
    {
        public IList<GameObject> GameObjects { get; set; }

        public Vector3 DisplacementVelocity { get; set; }

        public Vector3 RotationAxis { get; set; }

        public float AngularVelocity { get; set; }

        public bool UserControlled { get; set; }
    }
}
