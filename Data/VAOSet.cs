﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL4;

namespace NewWorld.Data
{
    public class VAOSet
    {
        public IList<GameObject> GameObjects { get; set; }

        public IDictionary<VAOType, VAO> VAOs { get; set; }

        public PrimitiveType PrimitiveType { get; set; }
    }
}
