﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

namespace NewWorld.Data
{
    public class FrameBuffer
    {
        public IList<GameObject> GameObjects { get; set; }

        public int FrameBufferId { get; set; }

        public int[] ColorAttachments { get; set; }
    }
}
