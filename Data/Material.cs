﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    public class Material
    {
        public IList<GameObject> GameObjects { get; set; }

        public float Ambient { get; set; }

        public float Diffuse { get; set; }

        public float Specular { get; set; }
    }
}
