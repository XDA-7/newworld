﻿using System.Collections.Generic;

using NewWorld.FactoryData.VertexData;

namespace NewWorld.Data
{
    public interface IDataSet
    {
        GodObject GodObject { get; set; }

        IList<GameObject> GameObjects { get; set; }

        IList<Transform> Transforms { get; set; }

        IList<VAOSet> VAOSets { get; set; }

        IList<Shader> Shaders { get; set; }

        IList<Camera> Cameras { get; set; }

        IList<Velocity> Velocities { get; set; }

        IList<Clock> Clocks { get; set; }

        IList<PointLight> PointLights { get; set; }

        IList<Texture> Textures { get; set; }

        IList<FrameBuffer> FrameBuffers { get; set; }

        IList<Material> Materials { get; set; }

        IList<ProceduralTexture> ProceduralTextures { get; set; }

        IDictionary<string, MeshData> MeshData { get; set; }
    }

    public class DataSet : IDataSet
    {
        public GodObject GodObject { get; set; }

        public IList<GameObject> GameObjects { get; set; }

        public IList<Transform> Transforms { get; set; }

        public IList<VAOSet> VAOSets { get; set; }

        public IList<Shader> Shaders { get; set; }

        public IList<Camera> Cameras { get; set; }

        public IList<Velocity> Velocities { get; set; }

        public IList<Clock> Clocks { get; set; }

        public IList<PointLight> PointLights { get; set; }

        public IList<Texture> Textures { get; set; }

        public IList<FrameBuffer> FrameBuffers { get; set; }

        public IList<Material> Materials { get; set; }

        public IList<ProceduralTexture> ProceduralTextures { get; set; }

        public IDictionary<string, MeshData> MeshData { get; set; }
    }
}
