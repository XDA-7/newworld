﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    /// <summary>
    /// Contains a Vertex Array Object and attributes
    /// </summary>
    public class VAO
    {
        public IList<GameObject> GameObjects { get; set; }

        public int VaoId { get; set; }

        /// <summary>
        /// Number of vertices in the array
        /// </summary>
        public int Size { get; set; }

        public IDictionary<string, int> VertexAttribs { get; set; }
    }
}
