﻿namespace NewWorld.Data
{
    public enum UniformType
    {
        model,
        view,
        proj,

        // Origin of the object being rendered
        objectPosition,

        // Origin of the renderer
        viewPosition,

        lightPosition,
        lightColor,
        lightIntensity,

        // The far plane specified in the projection matrix
        maxDist,

        // Views for a cubemap
        viewPosX,
        viewNegX,
        viewPosY,
        viewNegY,
        viewPosZ,
        viewNegZ,
        depthMap,

        // Material values
        ambient,
        diffuse,
        specular,

        // Texture values
        frameCount,
        cubeFace,
        textureCube,
        normalMap,

        // Procedural texture values
        scale
    }
}
