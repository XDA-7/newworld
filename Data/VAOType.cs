﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewWorld.Data
{
    public enum VAOType
    {
        Shadowmap, // [vec3 position]
        PositionNormal, // [vec3 position, vec3 normal]
        Lightmap, // [vec3 position, vec3 normal, vec3 uv]
        TangentSpace, // [vec3 position, vec3 normal, vec3 tangent, vec3 biTangent]
    }
}
