﻿#version 150

layout(points) in;
layout(triangle_strip, max_vertices = 96) out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
	int edges = 32;
	float angleDelta = radians(360.0 / float(edges));
	for (int i = 0; i < edges; i++)
	{
		float angle = angleDelta * i;
		gl_Position = proj * view * model * (gl_in[0].gl_Position + vec4(sin(angle), cos(angle), 0.0, 1.0));
		EmitVertex();
		gl_Position = proj * view * model * gl_in[0].gl_Position;
		EmitVertex();
		angle = angleDelta * (i - 1);
		gl_Position = proj * view * model * (gl_in[0].gl_Position + vec4(sin(angle), cos(angle), 0.0, 1.0));
		EmitVertex();
		EndPrimitive();
	}
}