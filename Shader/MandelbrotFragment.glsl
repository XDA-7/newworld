﻿#version 150

in vec3 Position;

uniform vec3 objectPosition;

out vec4 outColor;

vec2 complexSquare(vec2 value)
{
	return vec2(
		value.x * value.x - value.y * value.y,
		2.0 * value.x * value.y
	);
}

void main()
{
	float limit = 100.0;
	float magnification = objectPosition.z;
	vec2 baseVal = vec2(
		(Position.x * (1.0 / exp(magnification))) + objectPosition.x,
		(Position.y * (1.0 / exp(magnification))) + objectPosition.y);
	float iterations = 0.0;
	vec2 functionValue = vec2(0.0, 0.0);
	while (iterations < limit && length(functionValue) < 2.0)
	{
		functionValue = complexSquare(functionValue);
		functionValue = functionValue + baseVal;
		iterations = iterations + 1.0;
	}

	float finalVal = 1.0 - iterations / limit;
	outColor = vec4(finalVal, finalVal, finalVal, 1.0);
}