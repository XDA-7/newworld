﻿#version 150

in vec2 ScreenCoordinates;

uniform int cubeFace;
uniform samplerCube textureCube;

out vec4 outColor;

void main()
{
	vec3 accessVector;
	float x = ScreenCoordinates.x * 2.0;
	float y = ScreenCoordinates.y * 2.0;
	// Access order for the faces will be Z, X, -Z, -X, Y, -Y
	if (cubeFace == 0) // Z
	{
		accessVector = vec3(x, y, 1.0);
	}
	else if (cubeFace == 1) // X
	{
		accessVector = vec3(1.0, y, -x);
	}
	else if (cubeFace == 2) // -Z
	{
		accessVector = vec3(-x, y, -1.0);
	}
	else if (cubeFace == 3) // -X
	{
		accessVector = vec3(-1.0, y, x);
	}
	else if (cubeFace == 4) // Y
	{
		accessVector = vec3(x, 1.0, -y);
	}
	else if (cubeFace == 5) // -Y
	{
		accessVector = vec3(x, -1.0, y);
	}

	outColor = vec4(texture(textureCube, accessVector));
}