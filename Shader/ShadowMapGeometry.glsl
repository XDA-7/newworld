﻿#version 150

layout(triangles) in;
layout(triangle_strip, max_vertices = 18) out;

uniform mat4 viewPosX;
uniform mat4 viewNegX;
uniform mat4 viewPosY;
uniform mat4 viewNegY;
uniform mat4 viewPosZ;
uniform mat4 viewNegZ;
uniform mat4 proj;

out vec4 FragPosition;

void projectFace(int i, mat4 view)
{
	gl_Layer = i;
	for (int j = 0; j < 3; j++)
	{
		FragPosition = gl_in[j].gl_Position;
		gl_Position = proj * view * FragPosition;
		EmitVertex();
	}

	EndPrimitive();
}

void main()
{
	projectFace(0, viewPosX);
	projectFace(1, viewNegX);
	projectFace(2, viewPosY);
	projectFace(3, viewNegY);
	projectFace(4, viewPosZ);
	projectFace(5, viewNegZ);
}