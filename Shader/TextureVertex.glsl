﻿#version 150

in vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec3 Position;

void main()
{
	Position = position;
	gl_Position = proj * view * model * vec4(position, 1.0);
}