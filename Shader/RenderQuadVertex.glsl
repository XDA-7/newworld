﻿#version 150

in vec3 position;

out vec2 ScreenCoordinates;

void main()
{
	ScreenCoordinates = position.xy;
	gl_Position = vec4(position * 2.0, 1.0);
}