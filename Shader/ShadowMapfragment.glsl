﻿#version 150

in vec4 FragPosition;

uniform vec3 viewPosition;
uniform float maxDist;

out vec4 outColor;

void main()
{
	float distance = length(FragPosition.xyz - viewPosition) / maxDist;
	outColor = vec4(distance, 0.0f, 0.0f, 1.0f);
}