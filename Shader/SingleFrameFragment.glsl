﻿#version 150

in vec2 ScreenCoordinates;

uniform sampler2D frame0;

out vec4 outColor;

vec2 correctedCoords()
{
	return ScreenCoordinates + vec2(0.5, 0.5);
}

void main()
{
	outColor = vec4(texture(frame0, correctedCoords()));
}