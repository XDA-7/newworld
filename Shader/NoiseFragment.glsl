﻿#version 150

in vec2 ScreenCoordinates;

out vec4 outColor;

float scale = 2.0;
float randomVal = scale;
float a = mod((ScreenCoordinates.x) * scale, 1.0);
float b = mod((ScreenCoordinates.y) * scale, 1.0);
float bound = 1.0;

float rand()
{
	randomVal = mod(a * randomVal + b, bound);
	return randomVal;
}

void main()
{
	rand();
	outColor = vec4(randomVal, randomVal, randomVal, 1.0);
}