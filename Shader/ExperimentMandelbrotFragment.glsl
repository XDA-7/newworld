﻿#version 150

in vec3 Position;

uniform vec3 objectPosition;

out vec4 outColor;

struct Decimal
{
	int Significand;
	int Exponent;
};

struct DVec2
{
	Decimal x;
	Decimal y;
};

Decimal normalizeDecimal(Decimal decimal)
{
	while (decimal.Significand % 10 == 0)
	{
		decimal.Significand = decimal.Significand / 10;
		decimal.Exponent = decimal.Exponent + 1;
	}

	return decimal;
}

Decimal add(Decimal left, Decimal right)
{
	if (left.Exponent > right.Exponent)
	{
		left.Significand = left.Significand * int(pow(10, left.Exponent - right.Exponent));
		left.Exponent = right.Exponent;
	}
	else if (right.Exponent > left.Exponent)
	{
		right.Significand = right.Significand * int(pow(10, right.Exponent - left.Exponent));
		right.Exponent = left.Exponent;
	}

	Decimal sum = Decimal(left.Significand + right.Significand, left.Exponent);
	return normalizeDecimal(sum);
}

Decimal subtract(Decimal left, Decimal right)
{
	if (left.Exponent > right.Exponent)
	{
		left.Significand = left.Significand * int(pow(10, left.Exponent - right.Exponent));
		left.Exponent = right.Exponent;
	}
	else if (right.Exponent > left.Exponent)
	{
		right.Significand = right.Significand * int(pow(10, right.Exponent - left.Exponent));
		right.Exponent = left.Exponent;
	}

	Decimal sum = Decimal(left.Significand - right.Significand, left.Exponent);
	return normalizeDecimal(sum);
}

Decimal multiply(Decimal left, Decimal right)
{
	Decimal product = Decimal(
		left.Significand * right.Significand,
		left.Exponent + right.Exponent);
	return normalizeDecimal(product);
}

Decimal scalarMultiply(int scalar, Decimal decimal)
{
	Decimal product = Decimal(
		decimal.Significand * scalar,
		decimal.Exponent);
	return normalizeDecimal(product);
}

DVec2 complexSquare(DVec2 value)
{
	return DVec2(
		subtract(multiply(value.x, value.x), multiply(value.y, value.y)),
		scalarMultiply(2, multiply(value.x, value.y)));
}

void main()
{
	int limit = 100;
	int magnification = int(floor(objectPosition.z));
	vec2 baseVal = vec2(
		(Position.x * (1.0 / exp(magnification))) + objectPosition.x,
		(Position.y * (1.0 / exp(magnification))) + objectPosition.y);
	int iterations = 0;
	DVec2 functionValue = DVec2(Decimal(0, 0), Decimal(0, 0));
	while (iterations < limit && length(functionValue) < 2.0)
	{
		functionValue = complexSquare(functionValue);
		functionValue = functionValue + baseVal;
		iterations = iterations + 1.0;
	}

	float finalVal = 1.0 - iterations / limit;
	outColor = vec4(finalVal, finalVal, finalVal, 1.0);
}