﻿#version 150

layout(points) in;
layout(triangle_strip, max_vertices = 36) out;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec4 Normal;

void main()
{
	// Sides
	Normal = gl_in[0].gl_Position + vec4(-0.5f, -0.5f, -0.5f, 1.0f);
	gl_Position =  proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(-0.5f, 0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(0.5f, -0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(0.5f, 0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(0.5f, -0.5f, 0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(0.5f, 0.5f, 0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(-0.5f, -0.5f, 0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(-0.5f, 0.5f, 0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(-0.5f, -0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(-0.5f, 0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();

	EndPrimitive();

	// Top
	Normal = gl_in[0].gl_Position + vec4(-0.5f, 0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(-0.5f, 0.5f, 0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(0.5f, 0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(0.5f, 0.5f, 0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();

	EndPrimitive();

	// Bottom
	Normal = gl_in[0].gl_Position + vec4(-0.5f, -0.5f, 0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(-0.5f, -0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(0.5f, -0.5f, 0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();
	Normal = gl_in[0].gl_Position + vec4(0.5f, -0.5f, -0.5f, 1.0f);
	gl_Position = proj * view * model * Normal;
	EmitVertex();

	//EndPrimitive();
}