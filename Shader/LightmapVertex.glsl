﻿#version 150

in vec3 position;
in vec3 normal;
in vec2 uv;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform vec3 objectPosition;

out vec3 Position;
out vec3 Normal;
out vec2 UV;
out vec3 WorldPosition;

void main()
{
	UV = uv;
	Position = position;
	WorldPosition = vec3(model * vec4(position, 1.0));
	Normal = vec3(model * vec4(normal, 1.0)) - objectPosition;
	gl_Position = proj * view * model * vec4(position, 1.0);
}