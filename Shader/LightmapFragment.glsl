﻿#version 150

in vec3 Position;
in vec3 Normal;
in vec2 UV;
in vec3 WorldPosition;

uniform vec3 viewPosition;
uniform vec3 lightPosition;

uniform vec4 lightColor;
uniform float lightIntensity;

uniform float ambient;
uniform float diffuse;
uniform float specular;

uniform float maxDist;
uniform samplerCube depthMap;
uniform sampler2D normalMap;

out vec4 outColor;

float shadow()
{
	// Vector in the direction of the fragment from the light
	vec3 lightDirection = WorldPosition - lightPosition;
	// Use the light to fragment vector to sample from the depth map    
	float closestDepth = texture(depthMap, lightDirection).r;
	// It is currently in linear range between [0,1]. Re-transform back to original value
	closestDepth *= maxDist;
	// Now get current linear depth as the length between the fragment and light position
	float currentDepth = length(lightDirection);
	// Now test for shadows
	float bias = 0.05;
	float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;

	return 1 - shadow;
}

void main()
{
	vec3 unitNormal = normalize(Normal);
	vec3 lightDirection = normalize(lightPosition - WorldPosition);

	vec3 viewDirection = normalize(viewPosition - WorldPosition);
	vec3 lightReflection = reflect(-lightDirection, unitNormal);

	float diffuseVal = max(dot(unitNormal, lightDirection), 0.0f);
	float specularVal = pow(max(dot(viewDirection, lightReflection), 0.0f), 32.0f);

	outColor = vec4(lightColor.rgb * texture(normalMap, UV).rgb * (ambient + (diffuse * diffuseVal + specular * specularVal) * shadow()) * lightIntensity, 1.0);
}