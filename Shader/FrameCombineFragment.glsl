﻿#version 150

in vec2 ScreenCoordinates;

uniform int frameCount;
uniform sampler2D frame0;
uniform sampler2D frame1;
uniform sampler2D frame2;
uniform sampler2D frame3;
uniform sampler2D frame4;
uniform sampler2D frame5;
uniform sampler2D frame6;
uniform sampler2D frame7;

out vec4 outColor;

vec2 correctedCoords()
{
	return ScreenCoordinates + vec2(0.5, 0.5);
}

void main()
{
	outColor = vec4(0.0, 0.0, 0.0, 0.0);

	if (frameCount > 0)
	{
		outColor = outColor + vec4(texture(frame0, correctedCoords())) / frameCount;
	}
	if (frameCount > 1)
	{
		outColor = outColor + vec4(texture(frame1, correctedCoords())) / frameCount;
	}
	if (frameCount > 2)
	{
		outColor = outColor + vec4(texture(frame2, correctedCoords())) / frameCount;
	}
	if (frameCount > 3)
	{
		outColor = outColor + vec4(texture(frame3, correctedCoords())) / frameCount;
	}
	if (frameCount > 4)
	{
		outColor = outColor + vec4(texture(frame4, correctedCoords())) / frameCount;
	}
	if (frameCount > 5)
	{
		outColor = outColor + vec4(texture(frame5, correctedCoords())) / frameCount;
	}
	if (frameCount > 6)
	{
		outColor = outColor + vec4(texture(frame6, correctedCoords())) / frameCount;
	}
	if (frameCount > 7)
	{
		outColor = outColor + vec4(texture(frame7, correctedCoords())) / frameCount;
	}

	// outColor = outColor + vec4(0.1, 0.1, 0.1, 1.0);
}