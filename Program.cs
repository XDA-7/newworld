﻿using System;

using Autofac;

using NewWorld.Data;
using NewWorld.Factory;
using NewWorld.Process;

namespace NewWorld
{
    public class Program
    {
        private static IContainer Container;

        public static void Main(string[] args)
        {
            Run();
        }

        public static void Test()
        {
            var worldFactory = new WorldFactory();
            var world = worldFactory.BuildMeshes();

            Console.WriteLine(world["Frame"].PrimitiveType);
        }

        public static void Run()
        {
            ConfigureContainer();

            using (var scope = Container.BeginLifetimeScope())
            {
                var game = scope.Resolve<IGame>();
                game.Run();
            }
        }

        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Game>().As<IGame>().InstancePerLifetimeScope().PropertiesAutowired();
            builder.RegisterType<WindowDimensions>().As<IWindowDimensions>().InstancePerLifetimeScope().PropertiesAutowired();

            builder.RegisterType<DataSet>().As<IDataSet>().InstancePerLifetimeScope().PropertiesAutowired();

            // Data Factories
            builder.RegisterType<CameraFactory>().As<ICameraFactory>().InstancePerLifetimeScope();
            builder.RegisterType<ClockFactory>().As<IClockFactory>().InstancePerLifetimeScope();
            builder.RegisterType<FrameBufferFactory>().As<IFrameBufferFactory>().InstancePerLifetimeScope();
            builder.RegisterType<GameObjectFactory>().As<IGameObjectFactory>().InstancePerLifetimeScope();
            builder.RegisterType<GeometryFactory>().As<IGeometryFactory>().InstancePerLifetimeScope();
            builder.RegisterType<GodObjectFactory>().As<IGodObjectFactory>().InstancePerLifetimeScope();
            builder.RegisterType<MaterialFactory>().As<IMaterialFactory>().InstancePerLifetimeScope();
            builder.RegisterType<PointLightFactory>().As<IPointLightFactory>().InstancePerLifetimeScope();
            builder.RegisterType<ProceduralTextureFactory>().As<IProceduralTextureFactory>().InstancePerLifetimeScope();
            builder.RegisterType<ShaderFactory>().As<IShaderFactory>().InstancePerLifetimeScope();
            builder.RegisterType<TangentSpaceFactory>().As<ITangentSpaceFactory>().InstancePerLifetimeScope();
            builder.RegisterType<TextureFactory>().As<ITextureFactory>().InstancePerLifetimeScope();
            builder.RegisterType<TransformFactory>().As<ITransformFactory>().InstancePerLifetimeScope();
            builder.RegisterType<VAOFactory>().As<IVAOFactory>().InstancePerLifetimeScope();
            builder.RegisterType<VAOSetFactory>().As<IVAOSetFactory>().InstancePerLifetimeScope();
            builder.RegisterType<VelocityFactory>().As<IVelocityFactory>().InstancePerLifetimeScope();
            builder.RegisterType<WorldFactory>().As<IWorldFactory>().InstancePerLifetimeScope();

            // Processes
            builder.RegisterType<ClockUpdater>().As<IClockUpdater>().InstancePerLifetimeScope();
            builder.RegisterType<InputProcessor>().As<IInputProcessor>().InstancePerLifetimeScope();
            builder.RegisterType<LightMapRenderer>().As<ILightMapRenderer>().InstancePerLifetimeScope();
            builder.RegisterType<ModelGenerator>().As<IModelGenerator>().InstancePerLifetimeScope();
            builder.RegisterType<Renderer>().As<IRenderer>().InstancePerLifetimeScope();
            builder.RegisterType<ShadowMapRenderer>().As<IShadowMapRenderer>().InstancePerLifetimeScope();
            builder.RegisterType<VelocityCalculator>().As<IVelocityCalculator>().InstancePerLifetimeScope();
            builder.RegisterType<ViewGenerator>().As<IViewGenerator>().InstancePerLifetimeScope();
            builder.RegisterType<WorldBuilder>().As<IWorldBuilder>().InstancePerLifetimeScope().PropertiesAutowired();

            Container = builder.Build();
        }
    }
}
