﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace NewWorld
{
    public static class Util
    {
        public static void ErrorCheck(string location = null)
        {
            var error = GL.GetError();
            string errorMessage = "Unknown location: ";
            if (location != null)
            {
                errorMessage = "Error in " + location + ": ";
            }
            while (error != ErrorCode.NoError)
            {
                Console.WriteLine(errorMessage + error);
                error = GL.GetError();
            }
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, ignoreCase: true);
        }

        public static Vector3 Next(this Vector3[] circularArray, Vector3 x)
        {
            return circularArray[(Array.IndexOf(circularArray, x) + 1) % circularArray.Length];
        }

        public static Vector3 Last(this Vector3[] circularArray, Vector3 x)
        {
            return circularArray[(Array.IndexOf(circularArray, x) - 1) % circularArray.Length];
        }

        public static Vector3 SpherePoint(this Vector3 circlePoint, int level, float radius, int resolution)
        {
            // Not correct but may prove useful later
            // var levelRadius = ((1.0f + Math.Cos(Math.PI * ((double)level / resolution))) / 2.0f) * radius;
            var y = level * (radius / (resolution));
            var levelRadius = Math.Sqrt(radius * radius - y * y);
            var x = circlePoint.X * (float)levelRadius;
            var z = circlePoint.Z * (float)levelRadius;
            

            return new Vector3(x, y, z);
        }

        public static float[] Matrix4ToArray(IList<Matrix4> matrices)
        {
            var floats = new List<float>();
            foreach (var matrix in matrices)
            {
                floats.Add(matrix.M11);
                floats.Add(matrix.M12);
                floats.Add(matrix.M13);
                floats.Add(matrix.M14);
                floats.Add(matrix.M21);
                floats.Add(matrix.M22);
                floats.Add(matrix.M23);
                floats.Add(matrix.M24);
                floats.Add(matrix.M31);
                floats.Add(matrix.M32);
                floats.Add(matrix.M33);
                floats.Add(matrix.M34);
                floats.Add(matrix.M41);
                floats.Add(matrix.M42);
                floats.Add(matrix.M43);
                floats.Add(matrix.M44);
            }

            return floats.ToArray();
        }
    }
}
