﻿using System.Collections.Generic;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IProceduralTextureFactory
    {
        ProceduralTexture BuildNoise(int width, int height, float scale);
    }

    public class ProceduralTextureFactory : IProceduralTextureFactory
    {
        private IDataSet _dataSet;
        private IFrameBufferFactory _frameBufferFactory;
        private ITextureFactory _textureFactory;

        public ProceduralTextureFactory(IDataSet dataSet, IFrameBufferFactory frameBufferFactory, ITextureFactory textureFactory)
        {
            _dataSet = dataSet;
            _frameBufferFactory = frameBufferFactory;
            _textureFactory = textureFactory;
        }

        public ProceduralTexture BuildNoise(int width, int height, float scale)
        {
            // Generate and bind the texture
            var texture = _textureFactory.Build(TextureTarget.Texture2D, PixelFormat.Rgba, TextureWrapMode.Repeat, width, height);
            var textureBuffer = _frameBufferFactory.Build(new int[] { texture.TextureId }, false, false, width, height);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, texture.TextureId);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, textureBuffer.FrameBufferId);

            // Draw to texture with noise program
            var noiseProgram = _dataSet.GodObject.TextureGenerators.NoiseGenerator;

            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.UseProgram(noiseProgram.ProgramId);
            GL.Uniform1(noiseProgram.Uniforms[UniformType.scale], scale);

            var renderQuad = _dataSet.GodObject.RenderQuad;
            var vao = renderQuad.VAOs[VAOType.Shadowmap];
            GL.BindVertexArray(vao.VaoId);
            GL.DrawArrays(renderQuad.PrimitiveType, 0, vao.Size);
            GL.BindVertexArray(0);

            GL.UseProgram(0);
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            var proceduralTexture = new ProceduralTexture()
            {
                GameObjects = new List<GameObject>(),
                TextureBuffer = textureBuffer
            };

            _dataSet.ProceduralTextures.Add(proceduralTexture);
            return proceduralTexture;
        }
    }
}
