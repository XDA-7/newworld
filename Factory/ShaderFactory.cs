﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IShaderFactory
    {
        Shader Build(string vertexShaderPath, string fragmentShaderPath, string geometryShaderPath, string[] uniforms);
    }

    public class ShaderFactory : IShaderFactory
    {
        private IDataSet _dataSet { get; set; }

        public ShaderFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }
        
        public Shader Build(string vertexShaderPath, string fragmentShaderPath, string geometryShaderPath, string[] uniforms)
        {
            int program = GL.CreateProgram();
            GL.AttachShader(program, compileShader(ShaderType.VertexShader, vertexShaderPath));
            if (geometryShaderPath != null)
            {
                GL.AttachShader(program, compileShader(ShaderType.GeometryShader, geometryShaderPath));
            }

            GL.AttachShader(program, compileShader(ShaderType.FragmentShader, fragmentShaderPath));
            GL.LinkProgram(program);

            // Log program status
            var infoLog = new StringBuilder();
            var length = -1;
            GL.GetProgramInfoLog(program, 512, out length, infoLog);
            Console.WriteLine(infoLog.ToString());

            GL.UseProgram(program);
            var uniformLocations = new Dictionary<UniformType, int>();
            foreach (var uniform in uniforms)
            {
                var location = GL.GetUniformLocation(program, uniform);
                uniformLocations.Add(uniform.ToEnum<UniformType>(), location);
            }

            GL.UseProgram(0);

            var shader = new Shader()
            {
                GameObjects = new List<GameObject>(),
                ProgramId = program,
                Uniforms = uniformLocations
            };

            Util.ErrorCheck("Shader Factory");
            _dataSet.Shaders.Add(shader);
            return shader;
        }

        private int compileShader(ShaderType type, string path)
        {
            int shader = GL.CreateShader(type);
            using (var reader = new StreamReader(new FileStream(path, FileMode.Open, FileAccess.Read)))
            {
                GL.ShaderSource(shader, reader.ReadToEnd());
            }

            GL.CompileShader(shader);

            // Log compile status
            var infoLog = new StringBuilder();
            int length = -1;
            GL.GetShaderInfoLog(shader, 512, out length, infoLog);
            if (length != 0)
            {
                Console.WriteLine(string.Format("Error compiling shader at {0}", path));
                Console.WriteLine(infoLog.ToString());
            }

            return shader;
        }
    }
}
