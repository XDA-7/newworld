﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IClockFactory
    {
        Clock Build(bool active);
    }

    public class ClockFactory : IClockFactory
    {
        private IDataSet _dataSet { get; set; }

        public ClockFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public Clock Build(bool active)
        {
            var clock = new Clock()
            {
                GameObjects = new List<GameObject>(),
                Active = active,
                StartTime = DateTime.Now,
                LastTick = DateTime.Now,
                DeltaTime = 0.0f
            };

            _dataSet.Clocks.Add(clock);
            return clock;
        }
    }
}
