﻿using OpenTK;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface ICameraFactory
    {
        Camera Build(bool active, float minDist, float maxDist, float aspectRatio, float angle);
    }

    public class CameraFactory : ICameraFactory
    {
        private IDataSet _dataSet;

        public CameraFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public Camera Build(bool active, float minDist, float maxDist, float aspectRatio, float angle)
        {
            var camera = new Camera()
            {
                ViewValid = false,
                Projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(angle), aspectRatio, minDist, maxDist),
                MinDist = minDist,
                MaxDist = maxDist,
                Active = active
            };

            _dataSet.Cameras.Add(camera);
            return camera;
        }
    }
}
