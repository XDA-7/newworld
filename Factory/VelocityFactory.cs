﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IVelocityFactory
    {
        Velocity Build(Vector3 displacementVelocity, Vector3 rotationAxis, float angularVelocity, bool userControlled);
    }

    public class VelocityFactory : IVelocityFactory
    {
        private IDataSet _dataSet { get; set; }

        public VelocityFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public Velocity Build(Vector3 displacementVelocity, Vector3 rotationAxis, float angularVelocity, bool userControlled)
        {
            var velocity = new Velocity()
            {
                GameObjects = new List<GameObject>(),
                DisplacementVelocity = displacementVelocity,
                RotationAxis = rotationAxis,
                AngularVelocity = angularVelocity,
                UserControlled = userControlled
            };

            _dataSet.Velocities.Add(velocity);
            return velocity;
        }
    }
}
