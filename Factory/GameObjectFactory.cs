﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IGameObjectFactory
    {
        GameObject Build();
    }

    public class GameObjectFactory : IGameObjectFactory
    {
        private IDataSet _dataSet { get; set; }

        public GameObjectFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public GameObject Build()
        {
            var gameObject = new GameObject();
            _dataSet.GameObjects.Add(gameObject);
            return gameObject;
        }
    }
}
