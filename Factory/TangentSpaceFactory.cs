﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL4;

namespace NewWorld.Factory
{
    public interface ITangentSpaceFactory
    {
        float[,] Build(PrimitiveType primitiveType, float[] positions, float[] normals);
    }

    public class TangentSpaceFactory : ITangentSpaceFactory
    {
        public float[,] Build(PrimitiveType primitiveType, float[] positions, float[] normals)
        {
            if (primitiveType == PrimitiveType.Triangles)
            {
                return TrianglesBuild(positions, normals);
            }
            else
            {
                return null;
            }
        }

        private float[,] TrianglesBuild(float[] positions, float[] normals)
        {
            return null;
        }
    }
}
