﻿using System.Collections.Generic;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IMaterialFactory
    {
        Material Build(float ambient, float diffuse, float specular);
    }

    public class MaterialFactory : IMaterialFactory
    {
        private IDataSet _dataSet;

        public MaterialFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public Material Build(float ambient, float diffuse, float specular)
        {
            var material = new Material()
            {
                GameObjects = new List<GameObject>(),
                Ambient = ambient,
                Diffuse = diffuse,
                Specular = specular
            };

            _dataSet.Materials.Add(material);
            return material;
        }
    }
}
