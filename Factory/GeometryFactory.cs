﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;
using NewWorld.FactoryData.VertexData;

namespace NewWorld.Factory
{
    public interface IGeometryFactory
    {
        VAOSet BuildBox(float length, float width, float height);

        VAOSet BuildSphere(float radius, int resolution);
    }

    public class GeometryFactory : IGeometryFactory
    {
        private IVAOSetFactory _vaoSetFactory;

        // Note that a double array will not work here as select can only be called on a single rank array
        private readonly Vector2[] _flatFace = new Vector2[6]
            {
                 new Vector2() { Y =  1, X =  1 },
                 new Vector2() { Y =  1, X = -1 },
                 new Vector2() { Y = -1, X =  1 },
                 new Vector2() { Y =  1, X = -1 },
                 new Vector2() { Y = -1, X = -1 },
                 new Vector2() { Y = -1, X =  1 }
            };

        private readonly int[] test = new int[1] { 0 };

        public GeometryFactory(IVAOSetFactory vaoSetFactory)
        {
            _vaoSetFactory = vaoSetFactory;
        }

        public VAOSet BuildBox(float length, float width, float height)
        {
            // length = z, width = x, height = y
            length /= 2.0f;
            width /= 2.0f;
            height /= 2.0f;

            Func<Vector2, float[]> zPosFaceF = s => new float[] { s.X * width, s.Y * height, length };
            Func<Vector2, float[]> zNegFaceF = s => new float[] { s.Y * width, s.X * height, length };
            Func<Vector2, float[]> yPosFaceF = s => new float[] { s.Y * width, height, s.X * length };
            Func<Vector2, float[]> yNegFaceF = s => new float[] { s.X * width, height, s.Y * length };
            Func<Vector2, float[]> xPosFaceF = s => new float[] { width, s.X * height, s.Y * length };
            Func<Vector2, float[]> xNegFaceF = s => new float[] { width, s.Y * height, s.X * length };

            var zPosFace = _flatFace.SelectMany(zPosFaceF).ToArray();
            var zNegFace = _flatFace.SelectMany(zNegFaceF).Select(x => -x).ToArray();
            var yPosFace = _flatFace.SelectMany(yPosFaceF).ToArray();
            var yNegFace = _flatFace.SelectMany(yNegFaceF).Select(x => -x).ToArray();
            var xPosFace = _flatFace.SelectMany(xPosFaceF).ToArray();
            var xNegFace = _flatFace.SelectMany(xNegFaceF).Select(x => -x).ToArray();

            var positionVertices = zPosFace.Concat(zNegFace).Concat(yPosFace).Concat(yNegFace).Concat(xPosFace).Concat(xNegFace).ToArray();
            var positionSubSet = new VertexSubset
            {
                Attribute = AttributeType.Position,
                Size = 3,
                Vertices = positionVertices
            };

            var uvVertices = BasicUVCoordinates(positionVertices).SelectMany(x => new float[] { x.X, x.Y }).ToArray();
            var uvSubset = new VertexSubset
            {
                Attribute = AttributeType.UV,
                Size = 2,
                Vertices = uvVertices
            };

            return _vaoSetFactory.Build(PrimitiveType.Triangles, new VertexSubset[] { positionSubSet, uvSubset });
        }

        /// <summary>
        /// Currently only works with spehere radius of one
        /// </summary>
        public VAOSet BuildSphere(float radius, int resolution)
        {
            // Begin by generating a circle with the desired subdivision level
            var circle = new Vector3[]
            {
                Vector3.UnitX * radius,
                Vector3.UnitZ * radius,
                -Vector3.UnitX * radius,
                -Vector3.UnitZ * radius
            };

            for (int i = 0; i < resolution; i++)
            {
                // Add a new point between each existing point in the circle, normalized to the radius
                circle = circle.SelectMany(x => new Vector3[]
                {
                    x,
                    ((x + circle.Next(x)) /2.0f).Normalized() * radius
                }).ToArray();
            }

            var heightResolution = resolution * 4;

            var sphere = new List<Vector3>();
            for (int i = 0; i < heightResolution - 1; i++)
            {
                sphere.AddRange(circle.SelectMany(x => new Vector3[]
                {
                    x.SpherePoint(i, radius, heightResolution),
                    x.SpherePoint(i + 1, radius, heightResolution),
                    circle.Next(x).SpherePoint(i, radius, heightResolution),

                    x.SpherePoint(i + 1, radius, heightResolution),
                    circle.Next(x).SpherePoint(i + 1, radius, heightResolution),
                    circle.Next(x).SpherePoint(i, radius, heightResolution),

                    x.SpherePoint(-i, radius, heightResolution),
                    circle.Next(x).SpherePoint(-i, radius, heightResolution),
                    x.SpherePoint(-(i + 1), radius, heightResolution),

                    x.SpherePoint(-(i + 1), radius, heightResolution),
                    circle.Next(x).SpherePoint(-i, radius, heightResolution),
                    circle.Next(x).SpherePoint(-(i + 1), radius, heightResolution)
                }));
            }

            // Add circular caps to the top and bottom
            sphere.AddRange(circle.SelectMany(x => new Vector3[]
            {
                Vector3.UnitY * (radius - 1.0f / heightResolution),
                circle.Next(x).SpherePoint(heightResolution - 1, radius, heightResolution),
                x.SpherePoint(heightResolution - 1, radius, heightResolution),

                -Vector3.UnitY * (radius - 1.0f / heightResolution),
                x.SpherePoint(-(heightResolution - 1), radius, heightResolution),
                circle.Next(x).SpherePoint(-(heightResolution - 1), radius, heightResolution)
            }));

            // Because the shape is convex, every position is equal to its normal
            var positionVertices = sphere.SelectMany(x => new float[] { x.X, x.Y, x.Z }).ToArray();
            var positionSubset = new VertexSubset
            {
                Attribute = AttributeType.Position,
                Size = 3,
                Vertices = positionVertices
            };

            var uvVertices = BasicUVCoordinates(sphere).SelectMany(x => new float[] { x.X, x.Y }).ToArray();
            var uvSubset = new VertexSubset
            {
                Attribute = AttributeType.UV,
                Size = 2,
                Vertices = uvVertices
            };

            return _vaoSetFactory.Build(PrimitiveType.Triangles, new VertexSubset[] { positionSubset, uvSubset });
        }

        private List<Vector2> BasicUVCoordinates(List<Vector3> vectors)
        {
            return vectors.Select(x => BasicUVCoordinates(x)).ToList();
        }

        private List<Vector2> BasicUVCoordinates(float[] vertices)
        {
            var vectors = new List<Vector3>();
            for (int i = 0; i < vertices.Length; i += 3)
            {
                vectors.Add(new Vector3(vertices[i], vertices[i + 1], vertices[i + 2]));
            }

            return BasicUVCoordinates(vectors);
        }

        // Rotate the vector by 90 degree angles so that it's z component is positive and most significant
        // Return only the x, y components
        private Vector2 BasicUVCoordinates(Vector3 vector)
        {
            var x = vector.X;
            var y = vector.Y;
            var z = vector.Z;

            var absX = Math.Abs(x);
            var absY = Math.Abs(y);
            var absZ = Math.Abs(z);

            if (absX > absY && absX > absZ)
            {
                // +X
                if (x > 0.0)
                {
                    return new Vector2(-z, y);
                }
                // -X
                else
                {
                    return new Vector2(z, y);
                }
            }
            if (absY > absX && absY > absZ)
            {
                // +Y
                if (y > 0.0)
                {
                    return new Vector2(x, -z);
                }
                // -Y
                else
                {
                    return new Vector2(x, z);
                }
            }
            if (absZ > absX && absZ > absY)
            {
                // +Z
                if (z > 0.0)
                {
                    return new Vector2(x, y);
                }
                // -Z
                else
                {
                    return new Vector2(-x, y);
                }
            }

            Console.WriteLine("Error in BasicUVCoordinates method");
            return Vector2.Zero;
        }
    }
}
