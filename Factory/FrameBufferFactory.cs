﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IFrameBufferFactory
    {
        FrameBuffer Build(int[] colorAttachments, bool depthBuffer, bool stencilBuffer, int width, int height);
    }

    public class FrameBufferFactory : IFrameBufferFactory
    {
        private IDataSet _dataset;

        public FrameBufferFactory(IDataSet dataSet)
        {
            _dataset = dataSet;
        }

        public FrameBuffer Build(int[] colorAttachments, bool depthBuffer, bool stencilBuffer, int width, int height)
        {
            var frameBufferId = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBufferId);

            if (colorAttachments.Length > 16)
            {
                Console.WriteLine("ERROR: Framebuffer has too many color attachments");
            }

            for (int i = 0; i < colorAttachments.Length; i++)
            {
                GL.FramebufferTexture(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0 + i, colorAttachments[i], 0);
            }

            // Create and attach buffer
            var renderBuffer = GL.GenRenderbuffer();
            GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, renderBuffer);
            if (depthBuffer && stencilBuffer)
            {
                GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthStencil, width, height);
                GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthStencilAttachment, RenderbufferTarget.Renderbuffer, renderBuffer);
            }
            else if (depthBuffer)
            {
                GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.DepthComponent, width, height);
                GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.DepthAttachment, RenderbufferTarget.Renderbuffer, renderBuffer);
            }
            else if (stencilBuffer)
            {
                GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, RenderbufferStorage.StencilIndex8, width, height);
                GL.FramebufferRenderbuffer(FramebufferTarget.Framebuffer, FramebufferAttachment.StencilAttachment, RenderbufferTarget.Renderbuffer, renderBuffer);
            }

            // Status check
            var status = GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
            if (status != FramebufferErrorCode.FramebufferComplete)
            {
                Console.WriteLine(status);
            }

            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);

            var frameBuffer = new FrameBuffer()
            {
                GameObjects = new List<GameObject>(),
                FrameBufferId = frameBufferId,
                ColorAttachments = colorAttachments
            };

            Util.ErrorCheck("Framebuffer Factory");
            _dataset.FrameBuffers.Add(frameBuffer);
            return frameBuffer;
        }
    }
}
