﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface ITextureFactory
    {
        Texture Build(TextureTarget textureTarget, PixelFormat pixelFormat, TextureWrapMode textureWrapMode, int width, int height);
    }

    public class TextureFactory : ITextureFactory
    {
        private IDataSet _dataSet;

        public TextureFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public Texture Build(TextureTarget textureTarget, PixelFormat pixelFormat, TextureWrapMode textureWrapMode, int width, int height)
        {
            var textureId = GL.GenTexture();
            GL.BindTexture(textureTarget, textureId);
            
            GL.TexParameterI(textureTarget, TextureParameterName.TextureWrapS, new int[] { (int)textureWrapMode });
            GL.TexParameterI(textureTarget, TextureParameterName.TextureWrapT, new int[] { (int)textureWrapMode });
            GL.TexParameterI(textureTarget, TextureParameterName.TextureMagFilter, new int[] { (int)TextureMagFilter.Linear });
            GL.TexParameterI(textureTarget, TextureParameterName.TextureMinFilter, new int[] { (int)TextureMinFilter.Linear });
            GL.TexParameterI(textureTarget, TextureParameterName.TextureBaseLevel, new int[] { 0 });
            GL.TexParameterI(textureTarget, TextureParameterName.TextureMaxLevel, new int[] { 0 });

            // Cubemaps must define the wrap setting for the R dimension and initialise 6 images
            if (textureTarget == TextureTarget.TextureCubeMap)
            {
                GL.TexParameterI(textureTarget, TextureParameterName.TextureWrapR, new int[] { (int)textureWrapMode });
                for (int i = 0; i < 6; i++)
                {
                    GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0, (PixelInternalFormat)(int)pixelFormat, width, height, 0, pixelFormat, PixelType.UnsignedByte, IntPtr.Zero);
                }
            }
            else
            {
                GL.TexImage2D(textureTarget, 0, (PixelInternalFormat)(int)pixelFormat, width, height, 0, pixelFormat, PixelType.UnsignedByte, IntPtr.Zero);
            }

            GL.BindTexture(textureTarget, 0);

            var texture = new Texture()
            {
                GameObjects = new List<GameObject>(),
                TextureId = textureId,
                TextureTarget = textureTarget,
                PixelFormat = pixelFormat,
                TextureWrapMode = textureWrapMode,
                Width = width,
                Height = height
            };

            Util.ErrorCheck("Texture Factory");
            _dataSet.Textures.Add(texture);
            return texture;
        }
    }
}
