using System;
using System.Collections.Generic;
using System.IO;

using Newtonsoft.Json;

using NewWorld.FactoryData;
using NewWorld.FactoryData.VertexData;

namespace NewWorld.Factory
{
    public interface IWorldFactory
    {
        WorldData Build();

        Dictionary<string, MeshData> BuildMeshes();
    }

    public class WorldFactory : IWorldFactory
    {
        public WorldData Build()
        {
            var reader = new StreamReader(new FileStream("NewWorld.json", FileMode.Open, FileAccess.Read, FileShare.Read));
            return JsonConvert.DeserializeObject<WorldData>(reader.ReadToEnd());
        }

        public Dictionary<string, MeshData> BuildMeshes()
        {
            var result = new Dictionary<string, MeshData>();
            var di = new DirectoryInfo("Meshes");
            foreach (var file in di.GetFiles())
            {
                var reader = new StreamReader(new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read));
                result.Add(
                    file.Name.Split('.')[0],
                    JsonConvert.DeserializeObject<MeshData>(reader.ReadToEnd()));
            }

            return result;
        }
    }
}