﻿using System.Collections.Generic;

using OpenTK;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface ITransformFactory
    {
        Transform Build(Vector3 position, Vector3 rotation, float scale);
    }

    public class TransformFactory : ITransformFactory
    {
        private IDataSet _dataSet;

        public TransformFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public Transform Build(Vector3 position, Vector3 rotation, float scale)
        {
            var rotationQuat = Quaternion.FromEulerAngles(
                MathHelper.DegreesToRadians(rotation.X),
                MathHelper.DegreesToRadians(rotation.Y),
                MathHelper.DegreesToRadians(rotation.Z));

            var transform = new Transform()
            {
                GameObjects = new List<GameObject>(),
                Position = position,
                Rotation = rotationQuat,
                Scale = scale,
                ModelValid = false
            };

            _dataSet.Transforms.Add(transform);
            return transform;
        }
    }
}
