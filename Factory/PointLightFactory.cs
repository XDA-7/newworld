﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IPointLightFactory
    {
        PointLight Build(Color color, float intensity, int shadowmapResolution);
    }

    public class PointLightFactory : IPointLightFactory
    {
        private IDataSet _dataset;

        private ITextureFactory _textureFactory;

        private IFrameBufferFactory _frameBufferFactory;

        private IWindowDimensions _windowDimensions;

        public PointLightFactory(IDataSet dataSet, ITextureFactory textureFactory, IFrameBufferFactory frameBufferFactory, IWindowDimensions windowDimensions)
        {
            _dataset = dataSet;
            _textureFactory = textureFactory;
            _frameBufferFactory = frameBufferFactory;
            _windowDimensions = windowDimensions;
        }

        public PointLight Build(Color color, float intensity, int shadowmapResolution)
        {
            // Build a shadowmap
            // Shadowmaps require a cubemap color texture
            var shadowmapTexture = _textureFactory.Build(TextureTarget.TextureCubeMap, PixelFormat.Rgba, TextureWrapMode.ClampToEdge, shadowmapResolution, shadowmapResolution);
            var shadowmap = _frameBufferFactory.Build(new int[] { shadowmapTexture.TextureId }, true, false, shadowmapResolution, shadowmapResolution);

            // Build a lightmap
            int width = _windowDimensions.Width;
            int height = _windowDimensions.Height;
            var lightmapTexture = _textureFactory.Build(TextureTarget.Texture2D, PixelFormat.Rgba, TextureWrapMode.ClampToEdge, width, height);
            var lightmap = _frameBufferFactory.Build(new int[] { lightmapTexture.TextureId }, true, false, width, height);

            var pointLight = new PointLight()
            {
                Color = color,
                Intensity = intensity,
                ShadowmapResolution = shadowmapResolution,
                Shadowmap = shadowmap,
                Lightmap = lightmap
            };

            _dataset.PointLights.Add(pointLight);
            return pointLight;
        }
    }
}
