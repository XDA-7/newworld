﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IVAOFactory
    {
        VAO Build(Shader shader, float[] vertices, Dictionary<string, int> vertexAttribs);
    }

    public class VAOFactory : IVAOFactory
    {
        private IDataSet _dataSet { get; set; }

        public VAOFactory(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        /// <summary>
        /// Constructs and returns a VAO with attributes defined by the parameters
        /// </summary>
        public VAO Build(Shader shader, float[] vertices, Dictionary<string, int> vertexAttribs)
        {
            var vertexSize = vertexAttribs.Values.Sum();

            var vertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(vertexArrayObject);

            var vertexBuffer = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBuffer);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);

            // Get the locations of each vertex attribute
            var stride = vertexAttribs.Count == 1 ? 0 : vertexAttribs.Select(x => x.Value).Sum() * sizeof(float);
            var offset = 0;
            foreach (var vertexAttrib in vertexAttribs)
            {
                var attribSize = vertexAttrib.Value;
                var shaderProgramId = shader.ProgramId;
                var attrib = GL.GetAttribLocation(shaderProgramId, vertexAttrib.Key);
                GL.EnableVertexAttribArray(attrib);
                GL.VertexAttribPointer(attrib, attribSize, VertexAttribPointerType.Float, false, stride, offset);
                offset += attribSize * sizeof(float);
            }

            GL.BindVertexArray(0);

            var vao = new VAO()
            {
                GameObjects = new List<GameObject>(),
                VaoId = vertexArrayObject,
                Size = vertices.Length / vertexSize,
                VertexAttribs = vertexAttribs
            };

            Util.ErrorCheck("VAO Factory");
            return vao;
        }
    }
}
