﻿using System;
using System.Collections.Generic;
using System.Linq;

using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Factory
{
    public interface IGodObjectFactory
    {
        void CreateGod();
    }

    public class GodObjectFactory : IGodObjectFactory
    {
        private IDataSet _dataSet;
        private IClockFactory _clockFactory;
        private IShaderFactory _shaderFactory;
        private ITextureFactory _textureFactory;
        private IVAOSetFactory _vaoSetFactory;

        public GodObjectFactory(IDataSet dataSet, IClockFactory clockFactory, IShaderFactory shaderFactory, ITextureFactory textureFactory, IVAOSetFactory vaoSetFactory)
        {
            _dataSet = dataSet;
            _clockFactory = clockFactory;
            _shaderFactory = shaderFactory;
            _textureFactory = textureFactory;
            _vaoSetFactory = vaoSetFactory;
        }

        public void CreateGod()
        {
            var globalClock = _clockFactory.Build(true);
            var shadowmapShader = _shaderFactory.Build(
                @"Shader/ShadowMapVertex.glsl",
                @"Shader/ShadowMapFragment.glsl",
                @"Shader/ShadowMapGeometry.glsl",
                new string[]
                {
                    "model",
                    "viewPosX",
                    "viewNegX",
                    "viewPosY",
                    "viewNegY",
                    "viewPosZ",
                    "viewNegZ",
                    "proj",
                    "viewPosition",
                    "maxDist"
                });

            var lightmapShader = _shaderFactory.Build(
                @"Shader/LightmapVertex.glsl",
                @"Shader/LightmapFragment.glsl",
                null,
                new string[]
                {
                    "model",
                    "view",
                    "proj",
                    "objectPosition",
                    "lightPosition",
                    "lightColor",
                    "lightIntensity",
                    "ambient",
                    "diffuse",
                    "specular",
                    "maxDist",
                    "depthMap",
                    "normalMap"
                });

            var frameCombinerShader = _shaderFactory.Build(
                @"Shader/RenderQuadVertex.glsl",
                @"Shader/FrameCombineFragment.glsl",
                null,
                new string[]
                {
                    "frameCount"
                });

            var singleFrameShader = _shaderFactory.Build(
                @"Shader/RenderQuadVertex.glsl",
                @"Shader/SingleFrameFragment.glsl",
                null,
                new string[] { });

            var cubeFrameShader = _shaderFactory.Build(
                @"Shader/RenderQuadVertex.glsl",
                @"Shader/CubeFrameFragment.glsl",
                null,
                new string[]
                {
                    "cubeFace",
                    "textureCube"
                });

            var blankNormalMap = _textureFactory.Build(TextureTarget.Texture2D, PixelFormat.Rgb, TextureWrapMode.ClampToEdge, 100, 100);

            var textureGenerators = BuildTextureGenerators();

            _dataSet.GodObject = new GodObject()
            {
                GlobalClock = globalClock,
                ShadowmapShader = shadowmapShader,
                LightmapShader = lightmapShader,
                FrameCombineShader = frameCombinerShader,
                CubeFrameShader = cubeFrameShader,
                SingleFrameShader = singleFrameShader,
                UserInput = new UserInput(),
                BlankNormalMap = blankNormalMap,
                TextureGenerators = textureGenerators
            };

            // VAOs must use the lightmap shader in the god object, so they must be added after its instantiation
            var renderQuad = _vaoSetFactory.Build(_dataSet.MeshData["Frame"]);
            _dataSet.GodObject.RenderQuad = renderQuad;
        }

        private TextureGenerators BuildTextureGenerators()
        {
            var noiseGenerator = _shaderFactory.Build(
                @"Shader/RenderQuadVertex.glsl",
                @"Shader/SimplexNoise.glsl",
                null,
                new string[]
                {
                    "scale"
                });

            return new TextureGenerators()
            {
                NoiseGenerator = noiseGenerator
            };
        }
    }
}
