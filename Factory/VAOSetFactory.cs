﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;
using NewWorld.FactoryData.VertexData;

using Newtonsoft.Json.Linq;

namespace NewWorld.Factory
{
    public interface IVAOSetFactory
    {
        VAOSet Build(MeshData meshData);

        VAOSet Build(PrimitiveType primitiveType, VertexSubset[] vertexSubsets);
    }

    public class VAOSetFactory : IVAOSetFactory
    {
        private IDataSet _dataSet;
        private ITangentSpaceFactory _tangentSpaceFactory;
        private IVAOFactory _vaoFactory;

        public VAOSetFactory(IDataSet dataSet, ITangentSpaceFactory tangentSpaceFactory, IVAOFactory vaoFactory)
        {
            _dataSet = dataSet;
            _tangentSpaceFactory = tangentSpaceFactory;
            _vaoFactory = vaoFactory;
        }

        /// <summary>
        /// Build method which reads vertex data from a JSON file
        /// NOTE: For some anomalous reason, all of the JSON's that have been read in this method
        /// have caused the deserializer to throw an AccessViolationException
        /// the method has been kept as a curiosity, but should under no circumstances be called
        /// </summary>
        public VAOSet Build(string meshPath)
        {
            MeshData meshData = null;

            using (var reader = new StreamReader(new FileStream(meshPath, FileMode.Open, FileAccess.Read)))
            {
                meshData = JsonConvert.DeserializeObject<MeshData>(reader.ReadToEnd());
            }

            return Build(meshData.PrimitiveType.ToEnum<PrimitiveType>(), CreateVertexSubsets(meshData));
        }

        public VAOSet Build(MeshData meshData)
        {
            return Build(meshData.PrimitiveType.ToEnum<PrimitiveType>(), CreateVertexSubsets(meshData));
        }

        public VAOSet Build(PrimitiveType primitiveType, VertexSubset[] vertexSubsets)
        {
            var conversion = vertexSubsets.ToDictionary(x => x.Attribute);
            return Build(primitiveType, conversion);
        }

        private VAOSet Build(PrimitiveType primitiveType, Dictionary<AttributeType, VertexSubset> vertexSubsets)
        {
            if (!vertexSubsets.ContainsKey(AttributeType.Position))
            {
                Console.WriteLine("WARNING: A vertex set had no position attribute. Construction skipped.");
                return null;
            }

            // If no normals exist then just take the positions as normals
            if (!vertexSubsets.ContainsKey(AttributeType.Normal))
            {
                var positionVertices = vertexSubsets[AttributeType.Position].Vertices;
                vertexSubsets.Add(AttributeType.Normal, new VertexSubset
                {
                    Attribute = AttributeType.Normal,
                    Size = 3,
                    Vertices = new float[positionVertices.Length]
                });

                vertexSubsets[AttributeType.Position].Vertices.CopyTo(vertexSubsets[AttributeType.Normal].Vertices, 0);
            }

            var vaoSet = new VAOSet
            {
                GameObjects = new List<GameObject>(),
                VAOs = new Dictionary<VAOType, VAO>(),
                PrimitiveType = primitiveType
            };

            vaoSet.VAOs.Add(VAOType.Shadowmap, _vaoFactory.Build(_dataSet.GodObject.ShadowmapShader, vertexSubsets[AttributeType.Position].Vertices, new Dictionary<string, int> { { "position", 3 } }));

            if (vertexSubsets.ContainsKey(AttributeType.UV))
            {
                var lightmapVerts = MergeVertexSubSets(vertexSubsets[AttributeType.Position], vertexSubsets[AttributeType.Normal], vertexSubsets[AttributeType.UV]);
                vaoSet.VAOs.Add(VAOType.Lightmap, _vaoFactory.Build(_dataSet.GodObject.LightmapShader, lightmapVerts, new Dictionary<string, int> { { "position", 3 }, { "normal", 3 }, { "uv", 2 } }));
            }

            _dataSet.VAOSets.Add(vaoSet);
            return vaoSet;
        }

        private VertexSubset[] CreateVertexSubsets(MeshData meshData)
        {
            var result = new List<VertexSubset>();

            var offset = 0;
            var vertexSize = meshData.Attributes.Sum(x => x.Size);
            foreach(var attribute in meshData.Attributes)
            {
                var attributeSize = attribute.Size;
                var vertices = meshData.Vertices.Where((x, i) => i % vertexSize >= offset && i % vertexSize <= offset + attributeSize).ToArray();
                offset += attributeSize;

                result.Add(new VertexSubset
                {
                    Attribute = attribute.Name.ToEnum<AttributeType>(),
                    Size = attributeSize,
                    Vertices = vertices
                });
            }

            return result.ToArray();
        }

        private float[] MergeVertexSubSets(params VertexSubset[] subSets)
        {
            var result = new float[subSets.Select(x => x.Vertices.Length).Sum()];
            var vertexSize = subSets.Select(x => x.Size).Sum();
            var arrayOffset = 0;
            for (int i = 0; i < result.Length / vertexSize; i++)
            {
                foreach (var subSet in subSets)
                {
                    var subSetOffset = i * subSet.Size;
                    for (int j = 0; j < subSet.Size; j++)
                    {
                        result[arrayOffset] = subSet.Vertices[subSetOffset + j];
                        arrayOffset++;
                    }
                }
            }

            return result;
        }
    }
}
