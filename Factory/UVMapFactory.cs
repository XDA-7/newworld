﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

namespace NewWorld.Factory
{
    public interface IUVMapFactory
    {
        float[] Build(float[] mesh);
    }

    public class UVMapFactory : IUVMapFactory
    {
        public float[] Build(float[] mesh)
        {
            var triangles = new Vector3[mesh.Length / 9, 3];

            for (int i = 0; i < mesh.Length; i += 9)
            {
                triangles[i / 9, 0] = new Vector3(mesh[i], mesh[i + 1], mesh[i + 2]);
                triangles[i / 9, 1] = new Vector3(mesh[i + 3], mesh[i + 4], mesh[i + 5]);
                triangles[i / 9, 2] = new Vector3(mesh[i + 6], mesh[i + 7], mesh[i + 8]);
            }

            Vector3[] inOrderAngles;
            var angles = Angles(triangles, out inOrderAngles);

            return null;
        }

        private Dictionary<Vector3, List<TriangleAngles>> Angles(Vector3[,] triangles, out Vector3[] inOrderAngles)
        {
            var result = new Dictionary<Vector3, List<TriangleAngles>>();
            inOrderAngles = InOrderAngles(triangles);

            for (int i = 0; i < inOrderAngles.Length; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var point = triangles[i,j];
                    if (!result.ContainsKey(point))
                    {
                        result.Add(point, new List<TriangleAngles>());
                    }
                }

                var angles = inOrderAngles[i];
                result[triangles[i, 0]].Add(new TriangleAngles(angles[2], angles[0], angles[1]));
                result[triangles[i, 1]].Add(new TriangleAngles(angles[0], angles[1], angles[2]));
                result[triangles[i, 2]].Add(new TriangleAngles(angles[1], angles[2], angles[0]));
            }

            return result;
        }

        /// <summary>
        /// Provides the interior angles of each point on a single triangle as a Vector3
        /// </summary>
        private Vector3[] InOrderAngles(Vector3[,] triangles)
        {
            var angles = new Vector3[triangles.Length / 3];
            for (int i = 0; i < angles.Length; i++)
            {
                angles[i].X = Vector3.CalculateAngle(triangles[i, 1] - triangles[i, 0], triangles[i, 2] - triangles[i, 0]);
                angles[i].Y = Vector3.CalculateAngle(triangles[i, 0] - triangles[i, 1], triangles[i, 2] - triangles[i, 1]);
                angles[i].Z = Vector3.CalculateAngle(triangles[i, 0] - triangles[i, 2], triangles[i, 1] - triangles[i, 2]);
            }

            return angles;
        }

        private float ConstrainedMinimization(Vector3 angles)
        {
            return 0;
        }

        private double Minimization(Vector3[] unknownAngles, Vector3[] optimalAngles)
        {
            var result = 0.0;
            for (int i = 0; i < unknownAngles.Length; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    result += MinimizationInstance(unknownAngles[i][j], optimalAngles[i][j]);
                }
            }

            return result;
        }

        private double MinimizationInstance(float unknownAngle, float optimalAngle)
        {
            return Math.Pow(optimalAngle, 2.0f) * Math.Pow(unknownAngle - optimalAngle, 2.0f);
        }

        /// <summary>
        /// C tri
        /// </summary>
        private double TriangleValidity(Vector3[] unknownAngles, double[] multipliers)
        {
            return unknownAngles.Select((x, i) => multipliers[i] * (x.X + x.Y + x.Z - Math.PI)).Sum();
        }

        /// <summary>
        /// C plan
        /// ASSUMPTION: Dealing with convex shapes, all vertices are interior
        /// </summary>
        private double Planarity(List<float>[] unknownVertices, double[] multipliers)
        {
            var result = 0.0;
            for (int i = 0; i < unknownVertices.Length; i++)
            {
                result += multipliers[i] * unknownVertices[i].Select(x => x - 2.0 * Math.PI).Sum();
            }

            return result;
        }

        /// <summary>
        /// C len
        /// ASSUMPTION: Dealing with convex shapes, all vertices are interior
        /// </summary>
        private double Reconstruction(List<TriangleAngles>[] vertices, double[] multipliers)
        {
            return 0;
        }

        private struct TriangleAngles
        {
            public TriangleAngles(float previous, float current, float next)
            {
                Previous = previous;
                Current = current;
                Next = next;
            }

            float Previous;
            float Current;
            float Next;
        }
    }
}
