﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using NewWorld.Data;

namespace NewWorld.Process
{
    public interface IClockUpdater
    {
        void Run();
    }

    public class ClockUpdater : IClockUpdater
    {
        private IDataSet _dataSet { get; set; }

        public ClockUpdater(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public void Run()
        {
            foreach(var clock in _dataSet.Clocks)
            {
                clock.DeltaTime = (float)DateTime.Now.Subtract(clock.LastTick).Ticks / TimeSpan.TicksPerSecond;
                clock.LastTick = DateTime.Now;
            }
        }
    }
}
