﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

using NewWorld.Data;

namespace NewWorld.Process
{
    public interface IModelGenerator
    {
        /// <summary>
        /// Generates the Model matrices for all transforms
        /// </summary>
        void Run();
    }

    public class ModelGenerator : IModelGenerator
    {
        private IDataSet _dataSet;

        public ModelGenerator(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public void Run()
        {
            foreach (var transform in _dataSet.Transforms)
            {
                if (!transform.ModelValid)
                {
                    // For correct results the model should be scaled, then rotated, then moved
                    transform.Model = Matrix4.CreateScale(transform.Scale);
                    transform.Model = Matrix4.Mult(Matrix4.CreateFromQuaternion(transform.Rotation), transform.Model);
                    transform.Model = Matrix4.Mult(transform.Model, Matrix4.CreateTranslation(transform.Position));

                    transform.ModelValid = true;
                }
            }
        }
    }
}
