﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Process
{
    public interface IShadowMapRenderer
    {
        void Run();
    }

    public class ShadowMapRenderer : IShadowMapRenderer
    {
        private IDataSet _dataSet;

        public ShadowMapRenderer(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public void Run()
        {
            // Obtain the shadowmapping shader program and uniform positions
            if (_dataSet.PointLights.Count == 0)
            {
                return;
            }

            var shadowMapShaderProgram = _dataSet.GodObject.ShadowmapShader;
            var modelPosition = shadowMapShaderProgram.Uniforms[UniformType.model];
            var viewPosXPosition = shadowMapShaderProgram.Uniforms[UniformType.viewPosX];
            var viewNegXPosition = shadowMapShaderProgram.Uniforms[UniformType.viewNegX];
            var viewPosYPosition = shadowMapShaderProgram.Uniforms[UniformType.viewPosY];
            var viewNegYPosition = shadowMapShaderProgram.Uniforms[UniformType.viewNegY];
            var viewPosZPosition = shadowMapShaderProgram.Uniforms[UniformType.viewPosZ];
            var viewNegZPosition = shadowMapShaderProgram.Uniforms[UniformType.viewNegZ];
            var projPosition = shadowMapShaderProgram.Uniforms[UniformType.proj];
            // This is retarded
            var viewPositionPosition = shadowMapShaderProgram.Uniforms[UniformType.viewPosition];
            var maxDistPosition = shadowMapShaderProgram.Uniforms[UniformType.maxDist];
            
            GL.UseProgram(shadowMapShaderProgram.ProgramId);
            // Hardcoded maximum distance of the shadowmap render
            GL.Uniform1(maxDistPosition, 20.0f);
            // Light projection matrix, which should be the same across all lights
            var lightProj = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(90.0f), 1.0f, 0.5f, 20.0f);
            GL.UniformMatrix4(projPosition, false, ref lightProj);

            foreach (var pointLight in _dataSet.PointLights.Where(x => x.GameObject != null))
            {
                var gameObject = pointLight.GameObject;
                var lightPosition = gameObject?.Transform.Position ?? Vector3.Zero;

                // Generate the six views for the cubemap
                var up = -Vector3.UnitY;
                var forward = -Vector3.UnitZ;
                var lightView = Matrix4.LookAt(lightPosition, lightPosition + Vector3.UnitX, up);
                GL.UniformMatrix4(viewPosXPosition, false, ref lightView);
                lightView = Matrix4.LookAt(lightPosition, lightPosition - Vector3.UnitX, up);
                GL.UniformMatrix4(viewNegXPosition, false, ref lightView);
                lightView = Matrix4.LookAt(lightPosition, lightPosition + Vector3.UnitY, forward);
                GL.UniformMatrix4(viewPosYPosition, false, ref lightView);
                lightView = Matrix4.LookAt(lightPosition, lightPosition - Vector3.UnitY, forward);
                GL.UniformMatrix4(viewNegYPosition, false, ref lightView);
                lightView = Matrix4.LookAt(lightPosition, lightPosition + Vector3.UnitZ, up);
                GL.UniformMatrix4(viewPosZPosition, false, ref lightView);
                lightView = Matrix4.LookAt(lightPosition, lightPosition - Vector3.UnitZ, up);
                GL.UniformMatrix4(viewNegZPosition, false, ref lightView);

                GL.Uniform3(viewPositionPosition, gameObject.Transform.Position);

                var frameBuffer = pointLight.Shadowmap;
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, frameBuffer.FrameBufferId);

                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                GL.Enable(EnableCap.DepthTest);

                foreach (var renderObject in _dataSet.GameObjects.Where(x => x.VAOSet != null && x.Transform != null))
                {
                    var vao = renderObject.VAOSet.VAOs[VAOType.Shadowmap];
                    GL.BindVertexArray(vao.VaoId);
                    var modelMatrix = renderObject.Transform.Model;
                    GL.UniformMatrix4(modelPosition, false, ref modelMatrix);
                    GL.DrawArrays(renderObject.VAOSet.PrimitiveType, 0, vao.Size);
                    GL.BindVertexArray(0);
                }

                GL.Disable(EnableCap.DepthTest);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            }

            GL.UseProgram(0);
            Util.ErrorCheck("ShadowMap Renderer");
        }
    }
}
