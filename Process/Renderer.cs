﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Process
{
    public interface IRenderer
    {
        void Run();
    }

    public class Renderer : IRenderer
    {
        private IDataSet _dataSet { get; set; }

        public Renderer(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public void Run()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            // GL.ClearColor(Color.Black);
            GL.ClearColor(Color.White);
            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);
            GL.FrontFace(FrontFaceDirection.Ccw);

            var userInput = _dataSet.GodObject.UserInput;
            //SingleFrameDebug();
            if (userInput.Lightmap == 0 && userInput.Depthmap == 0)
            {
                FrameCombiner();
            }
            else if (userInput.Lightmap != 0)
            {
                SingleFrame();
            }
            else if (userInput.Depthmap != 0)
            {
                CubeFrame();
            }

            var renderQuad = _dataSet.GodObject.RenderQuad;
            var vao = renderQuad.VAOs[VAOType.Shadowmap];

            GL.BindVertexArray(vao.VaoId);
            GL.DrawArrays(renderQuad.PrimitiveType, 0, vao.Size);
            GL.BindVertexArray(0);

            GL.UseProgram(0);
            GL.Disable(EnableCap.CullFace);

            Util.ErrorCheck("Renderer");
        }

        /// <summary>
        /// Binds the frame combiner shader and sets up its uniforms
        /// </summary>
        private void FrameCombiner()
        {
            var frameCombineShader = _dataSet.GodObject.FrameCombineShader;
            GL.UseProgram(frameCombineShader.ProgramId);
            var pointLights = _dataSet.PointLights.ToArray();
            GL.Uniform1(frameCombineShader.Uniforms[UniformType.frameCount], pointLights.Length);
            for (int i = 0; i < pointLights.Length; i++)
            {
                GL.ActiveTexture(TextureUnit.Texture0 + i);
                GL.BindTexture(TextureTarget.Texture2D, pointLights[i].Lightmap.ColorAttachments[0]);
                var frameLocation = GL.GetUniformLocation(frameCombineShader.ProgramId, string.Format("frame{0}", i));
                GL.Uniform1(frameLocation, i);
            }
        }

        /// <summary>
        /// Binds the single frame shader and sets up its uniforms
        /// </summary>
        private void SingleFrame()
        {
            var singleFrameShader = _dataSet.GodObject.SingleFrameShader;
            GL.UseProgram(singleFrameShader.ProgramId);

            // Select texture for binding
            var userInput = _dataSet.GodObject.UserInput;
            var pointLights = _dataSet.PointLights.ToArray();
            var activeTexture = pointLights[userInput.Lightmap - 1].Lightmap.ColorAttachments[0];

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, activeTexture);
            var frameLocation = GL.GetUniformLocation(singleFrameShader.ProgramId, "frame0");
            GL.Uniform1(frameLocation, 0);
        }

        /// <summary>
        /// Setup to view a texture for debugging purposes
        /// </summary>
        private void SingleFrameDebug()
        {
            var singleFrameShader = _dataSet.GodObject.SingleFrameShader;
            GL.UseProgram(singleFrameShader.ProgramId);

            // Select texture for binding
            var activeTexture = _dataSet.ProceduralTextures.Single().TextureBuffer.ColorAttachments[0];

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, activeTexture);
            var frameLocation = GL.GetUniformLocation(singleFrameShader.ProgramId, "frame0");
            GL.Uniform1(frameLocation, 0);
        }

        /// <summary>
        /// Binds the cube frame shader and sets up its uniforms
        /// </summary>
        private void CubeFrame()
        {
            var cubeFrameShader = _dataSet.GodObject.CubeFrameShader;
            GL.UseProgram(cubeFrameShader.ProgramId);

            var userInput = _dataSet.GodObject.UserInput;
            var pointLights = _dataSet.PointLights.ToArray();
            var activeTexture = pointLights[userInput.Depthmap - 1].Shadowmap.ColorAttachments[0];

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.TextureCubeMap, activeTexture);
            GL.Uniform1(cubeFrameShader.Uniforms[UniformType.textureCube], 0);
            GL.Uniform1(cubeFrameShader.Uniforms[UniformType.cubeFace], userInput.DepthmapCubeSide - 1);
        }
    }
}
