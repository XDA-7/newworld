﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Input;

using NewWorld.Data;

namespace NewWorld.Process
{
    public interface IInputProcessor
    {
        void Run(Key[] keysDown, Key[] keysUp, Key[] keysHeld);
    }

    public class InputProcessor : IInputProcessor
    {
        private IDataSet _dataSet;

        private Dictionary<Key, int> _numberKeyMapping;

        public InputProcessor (IDataSet dataSet)
        {
            _dataSet = dataSet;
            _numberKeyMapping = NumberKeyMapping();
        }

        public void Run(Key[] keysDown, Key[] keysUp, Key[] keysHeld)
        {

            var movement = Vector3.Zero;
            var rotation = Vector3.Zero;

            var ctrlHeld = keysHeld.Contains(Key.ControlLeft);
            var shiftHeld = keysHeld.Contains(Key.ShiftLeft);

            foreach (var key in keysHeld)
            {
                if (key.Equals(Key.W))
                {
                    movement.Y += 1.0f;
                }
                else if (key.Equals(Key.S))
                {
                    movement.Y -= 1.0f;
                }
                else if (key.Equals(Key.A))
                {
                    movement.X += 1.0f;
                }
                else if (key.Equals(Key.D))
                {
                    movement.X -= 1.0f;
                }
                else if (key.Equals(Key.Space))
                {
                    movement.Z += 1.0f;
                }
                else if (key.Equals(Key.ControlLeft))
                {
                    movement.Z -= 1.0f;
                }
                else if (key.Equals(Key.Up))
                {
                    rotation.X -= 1.0f;
                }
                else if (key.Equals(Key.Down))
                {
                    rotation.X += 1.0f;
                }
                else if (key.Equals(Key.Left))
                {
                    rotation.Y += 1.0f;
                }
                else if (key.Equals(Key.Right))
                {
                    rotation.Y -= 1.0f;
                }

                // User selects which frame to display to screen
                if (_numberKeyMapping.ContainsKey(key))
                    FrameSelector(_numberKeyMapping[key], ctrlHeld, shiftHeld);
            }

            // Custom settings for Mandelbrot
            foreach (var velocity in _dataSet.Velocities)
            {
                if (velocity.UserControlled)
                {
                    // Use this for fractal navigation
                    ////var transform = velocity.GameObjects.First().Transform;
                    ////if (transform.Position.Z != 0)
                    ////{
                    ////    movement.X /= (float)(Math.Exp(transform.Position.Z));
                    ////    movement.Y /= (float)(Math.Exp(transform.Position.Z));
                    ////}
                    ////else
                    ////{
                    ////    movement.X /= 0.0001f;
                    ////    movement.Y /= 0.0001f;
                    ////}

                    velocity.DisplacementVelocity = movement;
                    velocity.RotationAxis = rotation;
                    velocity.AngularVelocity = 20.0f;
                }
            }
        }

        private void FrameSelector(int selection, bool selectDepthmap, bool selectDepthmapFace)
        {
            var userInput = _dataSet.GodObject.UserInput;
            if (selection == 0)
            {
                userInput.Lightmap = 0;
                userInput.Depthmap = 0;
                userInput.DepthmapCubeSide = 0;
            }
            
            else if (selectDepthmap)
            {
                userInput.Lightmap = 0;
                userInput.Depthmap = selection;
                userInput.DepthmapCubeSide = 1;
            }
            else if (selectDepthmapFace && selection > 0 && selection <= 6)
            {
                userInput.DepthmapCubeSide = selection;
            }
            else
            {
                userInput.Depthmap = 0;
                userInput.DepthmapCubeSide = 0;
                userInput.Lightmap = selection;
            }
        }

        private Dictionary<Key, int> NumberKeyMapping()
        {
            return new Dictionary<Key, int>()
            {
                { Key.Number0, 0 },
                { Key.Number1, 1 },
                { Key.Number2, 2 },
                { Key.Number3, 3 },
                { Key.Number4, 4 },
                { Key.Number5, 5 },
                { Key.Number6, 6 },
                { Key.Number7, 7 },
                { Key.Number8, 8 },
                { Key.Number9, 9 },
            };
        }
    }
}
