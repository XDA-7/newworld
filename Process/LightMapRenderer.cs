﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;

namespace NewWorld.Process
{
    public interface ILightMapRenderer
    {
        void Run();
    }

    public class LightMapRenderer : ILightMapRenderer
    {
        private IDataSet _dataSet;

        public LightMapRenderer(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public void Run()
        {
            if (_dataSet.PointLights.Count == 0)
            {
                return;
            }

            Camera activeCamera = _dataSet.Cameras.Where(x => x.Active).Single();
            Matrix4 view = activeCamera.View;
            Matrix4 projection = activeCamera.Projection;

            var lightMapShaderProgram = _dataSet.GodObject.LightmapShader;
            var modelPosition = lightMapShaderProgram.Uniforms[UniformType.model];
            var viewPosition = lightMapShaderProgram.Uniforms[UniformType.view];
            var projPosition = lightMapShaderProgram.Uniforms[UniformType.proj];
            var objectPositionPosition = lightMapShaderProgram.Uniforms[UniformType.objectPosition];
            var ambientPosition = lightMapShaderProgram.Uniforms[UniformType.ambient];
            var diffusePosition = lightMapShaderProgram.Uniforms[UniformType.diffuse];
            var specularPosition = lightMapShaderProgram.Uniforms[UniformType.specular];
            var lightPositionPosition = lightMapShaderProgram.Uniforms[UniformType.lightPosition];
            var lightColorPosition = lightMapShaderProgram.Uniforms[UniformType.lightColor];
            var lightIntensityPosition = lightMapShaderProgram.Uniforms[UniformType.lightIntensity];
            var maxDistPosition = lightMapShaderProgram.Uniforms[UniformType.maxDist];
            var depthMapPosition = lightMapShaderProgram.Uniforms[UniformType.depthMap];
            var normalMapPosition = lightMapShaderProgram.Uniforms[UniformType.normalMap];

            GL.UseProgram(lightMapShaderProgram.ProgramId);

            GL.Enable(EnableCap.CullFace);
            GL.Enable(EnableCap.DepthTest);

            // Guarantee default face-cull settings
            GL.CullFace(CullFaceMode.Back);
            GL.FrontFace(FrontFaceDirection.Ccw);

            // Camera-related uniforms can be applied immediately and used across all FBOs
            GL.UniformMatrix4(viewPosition, false, ref view);
            GL.UniformMatrix4(projPosition, false, ref projection);

            foreach (var pointLight in _dataSet.PointLights.Where(x => x.GameObject != null))
            {
                var lightColor = pointLight.Color;
                var lightIntensity = pointLight.Intensity;
                var lightPosition = pointLight.GameObject.Transform.Position;
                GL.Uniform4(lightColorPosition, lightColor);
                GL.Uniform1(lightIntensityPosition, lightIntensity);
                GL.Uniform3(lightPositionPosition, lightPosition);
                GL.Uniform1(maxDistPosition, 20.0f);

                // Bind depthMap
                GL.ActiveTexture(TextureUnit.Texture0);
                GL.BindTexture(TextureTarget.TextureCubeMap, pointLight.Shadowmap.ColorAttachments[0]);
                GL.Uniform1(depthMapPosition, 0);

                var lightmap = pointLight.Lightmap;
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, lightmap.FrameBufferId);

                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                GL.Enable(EnableCap.DepthTest);

                foreach (var renderObject in _dataSet.GameObjects.Where(x => x.VAOSet != null && x.Transform != null))
                {
                    var vao = renderObject.VAOSet.VAOs[VAOType.Lightmap];
                    GL.BindVertexArray(vao.VaoId);

                    var modelMatrix = renderObject.Transform.Model;
                    var objectPosition = renderObject.Transform.Position;
                    var material = renderObject.Material;
                    GL.UniformMatrix4(modelPosition, false, ref modelMatrix);
                    GL.Uniform3(objectPositionPosition, ref objectPosition);
                    GL.Uniform1(ambientPosition, material.Ambient);
                    GL.Uniform1(diffusePosition, material.Diffuse);
                    GL.Uniform1(specularPosition, material.Specular);

                    // Bind normal map
                    GL.ActiveTexture(TextureUnit.Texture1);
                    if (renderObject.ProceduralTexture != null)
                    {
                        GL.BindTexture(TextureTarget.Texture2D, renderObject.ProceduralTexture.TextureBuffer.ColorAttachments[0]);
                    }
                    else
                    {
                        GL.BindTexture(TextureTarget.Texture2D, _dataSet.GodObject.BlankNormalMap.TextureId);
                    }

                    GL.Uniform1(normalMapPosition, 1);

                    GL.DrawArrays(renderObject.VAOSet.PrimitiveType, 0, vao.Size);
                    GL.BindVertexArray(0);
                }

                GL.Disable(EnableCap.DepthTest);
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
            }

            GL.UseProgram(0);

            GL.Disable(EnableCap.CullFace);
            GL.Disable(EnableCap.DepthTest);

            Util.ErrorCheck("LightMapRenderer");
        }
    }
}
