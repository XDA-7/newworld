﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

using NewWorld.Data;

namespace NewWorld.Process
{
	public interface IViewGenerator
    {
        void Run();
    }

    public class ViewGenerator : IViewGenerator
    {
        private IDataSet _dataSet;

		public ViewGenerator(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

		public void Run()
        {
            var up = Vector3.UnitY;

			foreach(var camera in _dataSet.Cameras.Where(x => !x.ViewValid))
            {
                var transform = camera.GameObject.Transform;
                var position = (transform != null) ? transform.Position : Vector3.Zero;
                var rotation = (transform != null) ? transform.Rotation : Quaternion.Identity;
                var forward = Vector3.Transform(Vector3.UnitZ, Matrix4.CreateFromQuaternion(rotation));

                camera.View = Matrix4.LookAt(position, Vector3.Add(forward, position), up);
                camera.ViewValid = true;
            }
        }
    }
}
