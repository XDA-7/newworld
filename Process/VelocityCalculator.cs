﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;

using NewWorld.Data;

namespace NewWorld.Process
{
    /// <summary>
    /// Takes the velocity of each object and applies it to their transform
    /// </summary>
    public interface IVelocityCalculator
    {
        void Run();
    } 

    public class VelocityCalculator : IVelocityCalculator
    {
        private IDataSet _dataSet { get; set; }

        public VelocityCalculator(IDataSet dataSet)
        {
            _dataSet = dataSet;
        }

        public void Run()
        {
            // Obtain the global clock
            var clock = _dataSet.GodObject.GlobalClock;
            if (clock == null)
            {
                Console.WriteLine("Error: No active clock for the scene");
            }

            foreach (var velocity in _dataSet.Velocities)
            {
                foreach (var gameObject in _dataSet.GameObjects.Where(x => x.Velocity == velocity))
                {
                    if (gameObject.Transform != null)
                    {
                        if (gameObject.Camera != null)
                        {
                            gameObject.Camera.ViewValid = false;
                        }

                        var transform = gameObject.Transform;
                        transform.ModelValid = false;

                        var deltaPosition = Vector3.Multiply(velocity.DisplacementVelocity, clock.DeltaTime);
                        transform.Position = Vector3.Add(transform.Position, deltaPosition);

                        var deltaRotation = Quaternion.FromAxisAngle(velocity.RotationAxis, MathHelper.DegreesToRadians(velocity.AngularVelocity) * clock.DeltaTime);
                        transform.Rotation = Quaternion.Multiply(deltaRotation, transform.Rotation);
                    }
                }
            }
        }
    }
}
