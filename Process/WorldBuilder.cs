﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

using NewWorld.Data;
using NewWorld.Factory;
using NewWorld.FactoryData;
using NewWorld.FactoryData.GeometryData;

namespace NewWorld.Process
{
    public interface IWorldBuilder
    {
        ICameraFactory CameraFactory { get; set; }

        IClockFactory ClockFactory { get; set; }

        IDataSet DataSet { get; set; }

        IFrameBufferFactory FrameBufferFactory { get; set; }

        IGameObjectFactory GameObjectFactory { get; set; }

        IGeometryFactory GeometryFactory { get; set; }

        IMaterialFactory MaterialFactory { get; set; }

        IPointLightFactory PointLightFactory { get; set; }

        IProceduralTextureFactory ProceduralTextureFactory { get; set; }

        IShaderFactory ShaderFactory { get; set; }

        ITextureFactory TextureFactory { get; set; }

        ITransformFactory TransformFactory { get; set; }

        IVAOFactory VAOFactory { get; set; }

        IVAOSetFactory VAOSetFactory { get; set; }

        IVelocityFactory VelocityFactory { get; set; }

        IWorldFactory WorldFactory { get; set; }

        void Init();
    }

    public class WorldBuilder : IWorldBuilder
    {
        public ICameraFactory CameraFactory { get; set; }

        public IClockFactory ClockFactory { get; set; }

        public IDataSet DataSet { get; set; }

        public IFrameBufferFactory FrameBufferFactory { get; set; }

        public IGameObjectFactory GameObjectFactory { get; set; }

        public IGeometryFactory GeometryFactory { get; set; }

        public IGodObjectFactory GodObjectFactory { get; set; }

        public IMaterialFactory MaterialFactory { get; set; }

        public IPointLightFactory PointLightFactory { get; set; }

        public IProceduralTextureFactory ProceduralTextureFactory { get; set; }

        public IShaderFactory ShaderFactory { get; set; }

        public ITextureFactory TextureFactory { get; set; }

        public ITransformFactory TransformFactory { get; set; }

        public IVAOFactory VAOFactory { get; set; }

        public IVAOSetFactory VAOSetFactory { get; set; }

        public IVelocityFactory VelocityFactory { get; set; }

        public IWorldFactory WorldFactory { get; set; }

        public void Init()
        {
            DataSet.MeshData = WorldFactory.BuildMeshes();

            GodObjectFactory.CreateGod();

            var worldData = WorldFactory.Build();
            var components = worldData.Components;
            var cameras = BuildCameras(components.Cameras);
            var geometries = BuildGeometries(components.Geometries);
            var materials = BuildMaterials(components.Materials);
            var pointLights = BuildPointLights(components.PointLights);
            var proceduralTextures = BuildProceduralTextures(components.ProceduralTextures);
            var transforms = BuildTransforms(components.Transforms);
            var velocities = BuildVelocities(components.Velocities);

            foreach (var gameObjectDef in worldData.GameObjects)
            {
                var gameObject = GameObjectFactory.Build();

                if (!string.IsNullOrWhiteSpace(gameObjectDef.Camera))
                {
                    var camera = cameras[gameObjectDef.Camera];
                    gameObject.Camera = camera;
                    if (camera.GameObject != null)
                    {
                        Console.WriteLine("WARNING: A camera was assigned to multiple gameobjects");
                    }

                    camera.GameObject = gameObject;
                }

                if (!string.IsNullOrWhiteSpace(gameObjectDef.Geometry))
                {
                    var geometry = geometries[gameObjectDef.Geometry];
                    gameObject.VAOSet = geometry;
                    geometry.GameObjects.Add(gameObject);
                }

                if (!string.IsNullOrWhiteSpace(gameObjectDef.Material))
                {
                    var material = materials[gameObjectDef.Material];
                    gameObject.Material = material;
                    material.GameObjects.Add(gameObject);
                }

                if (!string.IsNullOrWhiteSpace(gameObjectDef.PointLight))
                {
                    var light = pointLights[gameObjectDef.PointLight];
                    gameObject.PointLight = light;

                    if (light.GameObject != null)
                    {
                        Console.WriteLine("WARNING: A pointlight was assigned to more that one object");
                    }

                    light.GameObject = gameObject;
                }

                if (!string.IsNullOrWhiteSpace(gameObjectDef.ProceduralTexture))
                {
                    var proceduralTexture = proceduralTextures[gameObjectDef.ProceduralTexture];
                    gameObject.ProceduralTexture = proceduralTexture;
                    proceduralTexture.GameObjects.Add(gameObject);
                }

                if (!string.IsNullOrWhiteSpace(gameObjectDef.Transform))
                {
                    var transform = transforms[gameObjectDef.Transform];
                    gameObject.Transform = transform;
                    transform.GameObjects.Add(gameObject);
                }

                if (!string.IsNullOrWhiteSpace(gameObjectDef.Velocity))
                {
                    var velocity = velocities[gameObjectDef.Velocity];
                    gameObject.Velocity = velocity;
                    velocity.GameObjects.Add(gameObject);
                }
            }
        }

        private Dictionary<string, Camera> BuildCameras(CameraData[] cameras)
        {
            var cameraDict = new Dictionary<string, Camera>();
            foreach (var camera in cameras)
            {
                cameraDict.Add(
                    camera.Id,
                    CameraFactory.Build(camera.Active, camera.MinDist, camera.MaxDist, camera.AspectRatio, camera.Angle));
            }

            return cameraDict;
        }

        /// <summary>
        /// Builds VAOs from dynamically generated geometries
        /// </summary>
        private Dictionary<string, VAOSet> BuildGeometries(GeometryData geometries)
        {
            var geometryDict = new Dictionary<string, VAOSet>();
            foreach (var box in geometries.Boxes)
            {
                geometryDict.Add(
                    box.Id,
                    GeometryFactory.BuildBox(box.Length, box.Width, box.Height)
                );
            }

            foreach (var sphere in geometries.Spheres)
            {
                geometryDict.Add(
                    sphere.Id,
                    GeometryFactory.BuildSphere(sphere.Radius, sphere.Resolution)
                );
            }

            return geometryDict;
        }

        private Dictionary<string, Material> BuildMaterials(MaterialData[] materials)
        {
            var materialDict = new Dictionary<string, Material>();
            foreach (var material in materials)
            {
                materialDict.Add(
                    material.Id,
                    MaterialFactory.Build(material.Ambient, material.Diffuse, material.Specular));
            }

            return materialDict;
        }

        private Dictionary<string, PointLight> BuildPointLights(PointLightData[] pointLights)
        {
            var lightDict = new Dictionary<string, PointLight>();
            foreach (var light in pointLights)
            {
                lightDict.Add(
                    light.Id,
                    PointLightFactory.Build(light.Color, light.Intensity, light.ShadowmapResolution));
            }

            return lightDict;
        }

        private Dictionary<string, ProceduralTexture> BuildProceduralTextures(ProceduralTextureData[] proceduralTextures)
        {
            var textureDict = new Dictionary<string, ProceduralTexture>();
            foreach (var texture in proceduralTextures)
            {
                if (texture.Type == "Noise")
                {
                    textureDict.Add(
                        texture.Id,
                        ProceduralTextureFactory.BuildNoise(texture.Width, texture.Height, texture.Scale));
                }
            }

            return textureDict;
        }

        private Dictionary<string, Transform> BuildTransforms(TransformData[] transforms)
        {
            var transformDict = new Dictionary<string, Transform>();
            foreach (var transform in transforms)
            {
                transformDict.Add(
                    transform.Id,
                    TransformFactory.Build(transform.Position, transform.Rotation, transform.Scale));
            }

            return transformDict;
        }

        private Dictionary<string, Velocity> BuildVelocities(VelocityData[] velocities)
        {
            var velocityDict = new Dictionary<string, Velocity>();
            foreach (var velocity in velocities)
            {
                velocityDict.Add(
                    velocity.Id,
                    VelocityFactory.Build(velocity.DisplacementVelocity, velocity.RotationAxis, velocity.AngularVelocity, velocity.UserControlled));
            }

            return velocityDict;
        }
    }
}
