namespace NewWorld.FactoryData
{
    public class ProceduralTextureData : FactoryData
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public string Type { get; set; }

        public float Scale { get; set; }
    }
}