﻿namespace NewWorld.FactoryData.VertexData
{
    public class VertexSubset
    {
        public AttributeType Attribute { get; set; }

        public int Size { get; set; }

        public float[] Vertices { get; set; }
    }
}
