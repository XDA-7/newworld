﻿namespace NewWorld.FactoryData.VertexData
{
    public enum AttributeType
    {
        Position,
        Normal,
        UV,
        Tangent,
        BiTangent
    }
}
