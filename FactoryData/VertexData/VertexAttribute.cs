﻿namespace NewWorld.FactoryData.VertexData
{
    public class VertexAttribute
    {
        public string Name { get; set; }

        public int Size { get; set; }
    }
}
