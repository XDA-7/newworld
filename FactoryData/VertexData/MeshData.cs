﻿namespace NewWorld.FactoryData.VertexData
{
    public class MeshData
    {
        public string PrimitiveType { get; set; }

        public VertexAttribute[] Attributes { get; set; }

        public float[] Vertices { get; set; }
    }
}
