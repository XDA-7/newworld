namespace NewWorld.FactoryData
{
    public class CameraData : FactoryData
    {
        public bool Active { get; set; }

        public float MinDist { get; set; }

        public float MaxDist { get; set; }

        public float AspectRatio { get; set; }

        public float Angle { get; set; }
    }
}