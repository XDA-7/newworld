using NewWorld.FactoryData.GeometryData;
using NewWorld.FactoryData.VertexData;

namespace NewWorld.FactoryData
{
    public class Components
    {
        public CameraData[] Cameras { get; set; }

        public MaterialData[] Materials { get; set; }

        public PointLightData[] PointLights { get; set; }

        public ProceduralTextureData[] ProceduralTextures { get; set; }

        public TransformData[] Transforms { get; set; }

        public VelocityData[] Velocities { get; set; }

        public GeometryData.GeometryData Geometries { get; set; }
    }
}