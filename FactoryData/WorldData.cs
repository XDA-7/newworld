using NewWorld.FactoryData.VertexData;

namespace NewWorld.FactoryData
{
    public class WorldData
    {
        public Components Components { get; set; }

        public GameObjectData[] GameObjects { get; set; }
    }
}