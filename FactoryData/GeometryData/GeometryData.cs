namespace NewWorld.FactoryData.GeometryData
{
    public class GeometryData
    {
        public BoxData[] Boxes { get; set; }

        public SphereData[] Spheres { get; set; }
    }
}