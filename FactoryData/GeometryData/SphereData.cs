namespace NewWorld.FactoryData.GeometryData
{
    public class SphereData : FactoryData
    {
        public float Radius { get; set; }

        public int Resolution { get; set; }
    }
}