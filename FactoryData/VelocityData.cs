using OpenTK;

namespace NewWorld.FactoryData
{
    public class VelocityData : FactoryData
    {
        public Vector3 DisplacementVelocity { get; set; }

        public Vector3 RotationAxis { get; set; }

        public float AngularVelocity { get; set; }

        public bool UserControlled { get; set; }
    } 
}