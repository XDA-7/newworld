using OpenTK;

namespace NewWorld.FactoryData
{
    public class PointLightData : FactoryData
    {
        public Color Color { get; set; }

        public float Intensity { get; set; }

        public int ShadowmapResolution { get; set; }
    }
}