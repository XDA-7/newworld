namespace NewWorld.FactoryData
{
    public class MaterialData : FactoryData
    {
        public float Ambient { get; set; }

        public float Diffuse { get; set; }

        public float Specular { get; set; }
    }
}