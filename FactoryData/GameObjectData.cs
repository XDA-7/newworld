namespace NewWorld.FactoryData
{
    public class GameObjectData : FactoryData
    {
        public string Camera { get; set; }

        public string Geometry { get; set; }

        public string Material { get; set; }

        public string Mesh { get; set; }

        public string PointLight { get; set; }

        public string ProceduralTexture { get; set; }

        public string Transform { get; set; }

        public string Velocity { get; set; }
    }
}