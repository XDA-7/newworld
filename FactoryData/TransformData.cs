using OpenTK;

namespace NewWorld.FactoryData
{
    public class TransformData : FactoryData
    {
        public Vector3 Position { get; set; }

        public Vector3 Rotation { get; set; }

        public float Scale { get; set; }
    }
}