namespace NewWorld.FactoryData
{
    public abstract class FactoryData
    {
        public string Id { get; set; }
    }
}