﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;

using NewWorld.Data;
using NewWorld.Process;

namespace NewWorld
{
    public interface IGame
    {
        IClockUpdater ClockUpdater { get; set; }

        ILightMapRenderer LightMapRenderer { get; set; }

        IModelGenerator ModelGenerator { get; set; }

        IRenderer Renderer { get; set; }

        IShadowMapRenderer ShadowMapRenderer { get; set; }

        IInputProcessor InputProcessor { get; set; }

        IVelocityCalculator VelocityCalculator { get; set; }

        IViewGenerator ViewGenerator { get; set; }

        IWorldBuilder WorldBuilder { get; set; }

        void Run();
    }

    public class Game : GameWindow, IGame
    {
        public IClockUpdater ClockUpdater { get; set; }

        public ILightMapRenderer LightMapRenderer { get; set; }

        public IModelGenerator ModelGenerator { get; set; }

        public IRenderer Renderer { get; set; }

        public IShadowMapRenderer ShadowMapRenderer { get; set; }

        public IInputProcessor InputProcessor { get; set; }

        public IVelocityCalculator VelocityCalculator { get; set; }

        public IViewGenerator ViewGenerator { get; set; }

        public IWorldBuilder WorldBuilder { get; set; }


        private List<Key> _keysDown { get; set; }

        private List<Key> _keysUp { get; set; }

        private List<Key> _keysHeld { get; set; }

        public Game(IWindowDimensions windowDimensions) : base(windowDimensions.Width, windowDimensions.Height)
        {
            _keysDown = new List<Key>();
            _keysUp = new List<Key>();
            _keysHeld = new List<Key>();

            Keyboard.KeyDown += KeyDown;
            Keyboard.KeyUp += KeyUp;
            GL.Viewport(0, 0, 1000, 1000);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            WorldBuilder.Init();
        }

        new void KeyDown(object sender, KeyboardKeyEventArgs e)
        {
            _keysDown.Add(e.Key);
            if(!_keysHeld.Contains(e.Key))
            {
                _keysHeld.Add(e.Key);
            }
        }

        new void KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            _keysUp.Add(e.Key);
            if (_keysHeld.Contains(e.Key))
            {
                _keysHeld.Remove(e.Key);
            }
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            InputProcessor.Run(_keysDown.ToArray(), _keysUp.ToArray(), _keysHeld.ToArray());
            _keysDown.Clear();
            _keysUp.Clear();

            ClockUpdater.Run();
            VelocityCalculator.Run();
            ModelGenerator.Run();
            ViewGenerator.Run();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            ShadowMapRenderer.Run();
            LightMapRenderer.Run();
            Renderer.Run();

            SwapBuffers();

            Util.ErrorCheck();
        }
    }
}
